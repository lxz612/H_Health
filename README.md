### H_Health-基于移动端的心理预约咨询平台

本产品主要功能有在线咨询、线上预约，心理专栏、心理周刊与心事问答等，为用户提供全方位的线上心理健康服务。

1.首页、心事专栏页

![首页和心事专栏](https://git.oschina.net/uploads/images/2017/1004/150049_b668a21b_502783.png "1.png")

2.心事问答页、个人中心页

![心事问答和个人中心](https://git.oschina.net/uploads/images/2017/1004/150140_ae1c511d_502783.png "2.png")

3.咨询师列表页、详情页

![输入图片说明](https://git.oschina.net/uploads/images/2017/1004/150847_a2d6252a_502783.png "Screenshot_1507097107.png") 
![输入图片说明](https://git.oschina.net/uploads/images/2017/1004/150855_e2c3e821_502783.png "Screenshot_1507097113.png")

4.订单页、支付页

![输入图片说明](https://git.oschina.net/uploads/images/2017/1004/150903_99295b73_502783.png "Screenshot_1507097186.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/1004/150913_04ff5743_502783.png "Screenshot_1507097208.png")

