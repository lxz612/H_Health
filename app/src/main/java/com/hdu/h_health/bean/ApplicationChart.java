package com.hdu.h_health.bean;

import cn.bmob.v3.BmobObject;

/**
 * 咨询师申请表
 * Created by 612 on 2016/3/20.
 */
public class ApplicationChart extends BmobObject{
    private User user;//申请用户名
    private Integer appliType;//申请类型，0--社会心理咨询师，1--高校心理咨询师
    private String name;
    private String sex;
    private String city;
    private String address;
    private String phone;
    private String email;
    private Integer type;//申请表状态，0--待审核，1--审核通过

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getAppliType() {
        return appliType;
    }

    public void setAppliType(Integer appliType) {
        this.appliType = appliType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
