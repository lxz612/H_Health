package com.hdu.h_health.bean;

import cn.bmob.v3.BmobUser;

/**
 * 扩展用户类
 * Created by 612 on 2016/2/27.
 */
public class User extends BmobUser {
    private Integer type;//用户类型 学生0 老师1 用户2 心理咨询师3
    //一般用户
    private String avatar;//头像
    private String nick;//昵称
    private Boolean sex;//性别
    private Integer age;//年龄
    private String summary;//个人签名,如果是咨询师，则是擅长的领域

    //认证为学生 老师
    private String province;//学校所在省份
    private String university;//大学
    private String college;//学院
    private Integer studentNumber;//学号
    private Integer teacherNumber;//工号

    //认证为心理咨询师
    private String city;//城市
    private String organization;//机构
    private String job;//职位

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public Integer getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(Integer studentNumber) {
        this.studentNumber = studentNumber;
    }

    public Integer getTeacherNumber() {
        return teacherNumber;
    }

    public void setTeacherNumber(Integer teacherNumber) {
        this.teacherNumber = teacherNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
