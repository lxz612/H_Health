package com.hdu.h_health.bean;

/**
 * 新闻列表数据数据类---仅供测试
 * Created by 612 on 2015/10/5.
 */
public class NewsBean extends BaseBean{
    private Integer type;
    private String title;
    private String content;
    private String url;
    private String picurl;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }
}
