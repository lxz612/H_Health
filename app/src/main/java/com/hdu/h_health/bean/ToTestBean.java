package com.hdu.h_health.bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by See on 2016/3/23.
 */
public class ToTestBean extends BmobObject{
    public String question;
    public String option1;
    public String option2;
    public String option3;
    public String answer;
//    public String option;


    public ToTestBean() {
        this.setTableName("tab");
    }

    public ToTestBean(String question, String option1, String option2, String option3) {
        this.option1 = option1;
//        this.option = option;
        this.question = question;
        this.option2 = option2;
        this.option3 = option3;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}