package com.hdu.h_health.bean;

/**
 * 首页周刊列表Bean
 * Created by See on 2016/4/5.
 */
public class WeeklyBean extends BaseBean{
    private String title;//周刊标题
    private String picUrl;//周刊图片
    private String source;//来源

    public WeeklyBean(String mPicture, String title) {
        this.picUrl = mPicture;
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
