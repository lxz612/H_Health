package com.hdu.h_health.bean;

import cn.bmob.v3.BmobObject;

/**
 * 心事评论类
 * Created by 612 on 2016/3/22.
 */
public class Comment extends BmobObject{
    private Secret secret;//评论对应的心事
    private User author;//评论该心事的人
    private String content;//评论

    public Secret getSecret() {
        return secret;
    }

    public void setSecret(Secret secret) {
        this.secret = secret;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
