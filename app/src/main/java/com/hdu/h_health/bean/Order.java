package com.hdu.h_health.bean;

import cn.bmob.v3.BmobObject;

/**
 * 订单类
 * Created by 612 on 2016/4/3.
 */
public class Order extends BmobObject{
    private String orderNumber;
    private Integer type;//订单状态 0-待付款 1-已付款 2-取消订单 3-待评价
    private Integer conType;//预约类型 0-语音 1-视频 2-面对面
    private Integer sum;//次数
    private Integer acount;//金额
    private String comment;//评价
    private Integer star;//评价星级
    private User author;//生成订单人
    private User counsult;//预约咨询师
    private String call;//称呼
    private String age;//年龄
    private Boolean sex;//性别
    private String phoneNumber;//手机号
    private String queDescription;//问题描述
    private boolean isPass;//是否同意咨询协议

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getConType() {
        return conType;
    }

    public void setConType(Integer conType) {
        this.conType = conType;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public Integer getAcount() {
        return acount;
    }

    public void setAcount(Integer acount) {
        this.acount = acount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getCounsult() {
        return counsult;
    }

    public void setCounsult(User counsult) {
        this.counsult = counsult;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getQueDescription() {
        return queDescription;
    }

    public void setQueDescription(String queDescription) {
        this.queDescription = queDescription;
    }

    public boolean isPass() {
        return isPass;
    }

    public void setIsPass(boolean isPass) {
        this.isPass = isPass;
    }
}
