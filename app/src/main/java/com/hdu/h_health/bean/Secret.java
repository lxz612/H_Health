package com.hdu.h_health.bean;

import java.io.Serializable;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobRelation;

/**
 * 心事类(心事找不到好的翻译--暂时用Secret)
 * Created by 612 on 2016/3/22.
 */
public class Secret extends BmobObject implements Serializable {

    private User author;//心事作者
    private String content;//内容
    private String imgUrl;//帖子图片
    private Integer love;//喜欢
    private Integer share;//分享
    private Integer comment;//评论
    private Boolean isPass;//是否通过
    private Boolean myFav;//收藏
    private Boolean myLove;//赞
    private BmobRelation commentRe;//关系----->用于建立心事与评论之间一对多的关联

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getLove() {
        return love;
    }

    public void setLove(Integer love) {
        this.love = love;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public Boolean getIsPass() {
        return isPass;
    }

    public void setIsPass(Boolean isPass) {
        this.isPass = isPass;
    }

    public Boolean getMyFav() {
        return myFav;
    }

    public void setMyFav(Boolean myFav) {
        this.myFav = myFav;
    }

    public Boolean getMyLove() {
        return myLove;
    }

    public void setMyLove(Boolean myLove) {
        this.myLove = myLove;
    }

    public BmobRelation getCommentRe() {
        return commentRe;
    }

    public void setCommentRe(BmobRelation commentRe) {
        this.commentRe = commentRe;
    }
}
