package com.hdu.h_health.event;

/**
 * 个人消息更新事件
 * Created by 612 on 2016/4/24.
 */
public class UpdateEvent {
    private int type;
    private String message;

    public UpdateEvent(int type, String message){
        this.type=type;
        this.message=message;
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
