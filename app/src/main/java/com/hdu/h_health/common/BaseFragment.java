package com.hdu.h_health.common;

import android.support.v4.app.Fragment;

import com.hdu.h_health.MyApplication;
import com.squareup.leakcanary.RefWatcher;

/**
 * 实现懒加载的Fragment
 * Created by 612 on 2016/3/11.
 */
public abstract class BaseFragment extends Fragment {

    public boolean isVisible;//判断是否可见

    /**
     * Fragment可见时，setUserVisibleHint在onCreateView前调用
     *
     * @param isVisibleToUser true--Fragment的UI是可见的
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }

    /**
     * 可见时调用
     */
    protected void onVisible() {
        lazyLoad();
    }

    protected abstract void lazyLoad();

    /**
     * 不可见时调用
     */
    protected void onInvisible() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MyApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }
}
