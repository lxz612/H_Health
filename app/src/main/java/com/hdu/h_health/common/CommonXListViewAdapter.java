package com.hdu.h_health.common;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hdu.h_health.internet.ImageLoadUsingUIL;
import com.hdu.h_health.internet.ImageLoadUsingVolley;

import java.util.List;

/**
 * XListView工具类--公共的XListView适配器
 * Created by 612 on 2016/2/26.
 */
public abstract class CommonXListViewAdapter<T> extends BaseAdapter {

    protected Context context;//上下文
    protected List<T> mNewsData;//新闻实体类数据数组
    //图片下载类
    protected ImageLoadUsingVolley imageLoadUsingVolley;
    protected ImageLoadUsingUIL imageLoadUsingUIL;

    protected AssetManager mgr;//获取Assets中的资源
    protected Typeface HYQiHei;//汉仪旗黑字体

    //标识两种不同的ListView列表项类型
    protected static final int TYPE_FIRST = 0;
    protected static final int TYPE_SECOND = 1;

    /**
     * 初始化CommonXListViewAdapter时调用的构造方法
     */
    public CommonXListViewAdapter(Context context, List<T> mNewsData) {
        this.context = context;
        this.mNewsData = mNewsData;
        imageLoadUsingVolley = new ImageLoadUsingVolley();
        imageLoadUsingUIL =new ImageLoadUsingUIL();
        mgr = context.getAssets();
        HYQiHei = Typeface.createFromAsset(mgr, "fonts/HYQiHei-60S.otf");
    }

    /**
     * ListView列表项布局类型数量
     */
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    /**
     * ListView列表项布局类型--需子类具体实现
     */
    @Override
    public abstract int getItemViewType(int position);

    /**
     * 新闻数据数组的长度
     */
    @Override
    public int getCount() {
        return mNewsData.size();
    }

    /**
     * 返回新闻数据数组中的对象
     */
    @Override
    public Object getItem(int position) {
        return mNewsData.get(position);
    }

    /**
     * 返回新闻数据数组中的对象Id
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 绘制每一个列表项时---需子类具体实现该方法
     */
    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);
}
