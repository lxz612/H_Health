package com.hdu.h_health.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Config;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hdu.h_health.MyApplication;
import com.hdu.h_health.R;
import com.hdu.h_health.ui.LoginActivity;
import com.hdu.h_health.widget.NavigationView;
import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.RefWatcher;

import de.greenrobot.event.EventBus;

/**
 * 基类
 * Created by 612 on 2016/3/8.
 */
public class BaseActivity extends AppCompatActivity {

    protected String TAG = "Main";
    protected NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 我设置了两个动画资源文件作为启动动画和退出动画
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        super.onCreate(savedInstanceState);
        //将创建的Activity放入ActivityManagerUtils工具的Activity的数组中
        MyApplication.getInstance().addActivity(this);
        //注册EventBus
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //反注册EventBus
        EventBus.getDefault().unregister(this);
        RefWatcher refWatcher=MyApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initView();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        initView();
    }

    //EventBus事件
    public void onEvent(Boolean empty) {

    }

    protected void initView() {
    }

    protected void runOnMain(Runnable runnable) {
        runOnUiThread(runnable);
    }

    //Toast封装
    protected final static String NULL = "";
    private Toast toast;

    public void toast(final Object obj) {
        try {
            runOnMain(new Runnable() {

                @Override
                public void run() {
                    if (toast == null)
                        toast = Toast.makeText(BaseActivity.this, NULL, Toast.LENGTH_SHORT);
                    toast.setText(obj.toString());
                    toast.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startActivity(Class<? extends Activity> target, Bundle bundle, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(this, target);
        if (bundle != null)
            intent.putExtra(getPackageName(), bundle);
        startActivity(intent);
        if (finish)
            finish();
    }

    //获取Bundle对象
    public Bundle getBundle() {
        if (getIntent() != null && getIntent().hasExtra(getPackageName()))
            return getIntent().getBundleExtra(getPackageName());
        else
            return null;
    }


    /**
     * 隐藏软键盘
     */
    public void hideSoftInputView() {
        InputMethodManager manager = ((InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE));
        if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            if (getCurrentFocus() != null)
                manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 隐藏软键盘-一般是EditText.getWindowToken()
     *
     * @param token
     */
    public void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Log日志
     *
     * @param msg
     */
    public void log(String msg) {
        if (Config.DEBUG) {
            Logger.i(msg);
        }
    }

    /**
     * 显示圆形进度条
     *
     * @param context
     */
    public void showProgressDialog(Context context) {
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage("正在登陆...");
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }
}
