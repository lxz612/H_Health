package com.hdu.h_health.model;

import android.content.Context;
import android.widget.BaseAdapter;

import com.hdu.h_health.bean.Comment;
import com.hdu.h_health.bean.Secret;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.listener.CountListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

/**
 * 评论操作类
 * Created by 612 on 2016/3/23.
 */
public class CommentModel extends BaseModel {

    private static CommentModel commentModel = new CommentModel();

    public static CommentModel getInstance() {
        return commentModel;
    }

    //添加评论
    public void addComment(final Context context, String content, User user, Comment comment,SaveListener saveListener) {
        comment.setContent(content);
        //添加心事与心事评论者的关系（一对一）
        comment.setAuthor(user);
        comment.save(context,saveListener);
    }

    private int number;

    //查询评论数量
    public int queryNumber(final Context context, Secret secret) {
        BmobQuery<Comment> query = new BmobQuery<Comment>();
        query.addWhereEqualTo("secret", secret.getObjectId());
        query.count(context, Comment.class, new CountListener() {
            @Override
            public void onSuccess(int i) {
                number = i;
                CommonUtils.showToast(context, i + "");
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(context, "查询失败" + i + s);
            }
        });
        return number;
    }
}
