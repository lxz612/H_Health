package com.hdu.h_health.model.i;

import com.hdu.h_health.bean.User;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.BmobListener;

/**
 * 查询用户监听器
 * Created by 612 on 2016/3/8.
 */
public abstract class QueryUserListener extends BmobListener<User> {
    public abstract void done(User s, BmobException e);
    @Override
    protected void postDone(User o, BmobException e) {
        done(o, e);
    }
}
