package com.hdu.h_health.model;

import android.content.Context;

import com.hdu.h_health.bean.Order;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.utils.CommonUtils;

import cn.bmob.v3.listener.SaveListener;

/**
 * Created by 612 on 2016/4/3.
 */
public class OrderModel extends BaseModel {
    private static OrderModel orderModel = new OrderModel();

    public static OrderModel getInstance() {
        return orderModel;
    }

    public void addOrder(final Context context,User consult,int type, int acount,String call,
                         String age, boolean sex, String phone, String ques) {
        Order order=new Order();
        order.setOrderNumber(" ");
        order.setType(type);
        order.setAcount(acount);
        order.setCall(call);
        order.setAge(age);
        order.setSex(sex);
        order.setPhoneNumber(phone);
        order.setQueDescription(ques);
        order.setAuthor(UserModel.getInstance().getCurrentUser());
        order.setCounsult(consult);
        order.setComment("");
        order.setStar(1);
        order.save(context, new SaveListener() {
            @Override
            public void onSuccess() {
                CommonUtils.showToast(context,"");
            }

            @Override
            public void onFailure(int i, String s) {

            }
        });
    }
}
