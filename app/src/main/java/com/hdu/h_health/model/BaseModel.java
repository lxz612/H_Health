package com.hdu.h_health.model;

import android.content.Context;

import com.hdu.h_health.MyApplication;
import com.hdu.h_health.bean.User;

import cn.bmob.v3.BmobUser;

/**
 * 基类
 * Created by 612 on 2016/3/8.
 */
public class BaseModel {

    //错误码
    public int CODE_NULL = 1000;
    public static int CODE_NOT_EQUAL = 1001;
    public static final int DEFAULT_LIMIT = 20;

    public Context getContext() {
        return MyApplication.getInstance();
    }

    /**
     * 获取当前用户
     */
    public User getCurrentUser(){
        return BmobUser.getCurrentUser(getContext(), User.class);
    }
}
