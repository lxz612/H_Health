package com.hdu.h_health.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hdu.h_health.R;

/**
 * 导航栏
 * Created by 612 on 2016/3/26.
 */
public class NavigationView extends LinearLayout implements View.OnClickListener {
    private ImageView iv_left;
    private ImageView iv_right;
    private TextView tv_title;

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.navigation_view, this,true);
        iv_left = (ImageView) view.findViewById(R.id.iv_left);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        iv_right = (ImageView) view.findViewById(R.id.iv_right);
        iv_left.setOnClickListener(this);
        iv_right.setOnClickListener(this);
    }

    public ImageView getIv_left() {
        return iv_left;
    }

    public ImageView getIv_right() {
        return iv_right;
    }


    public void setTv_title(String title) {
        tv_title.setText(title);
    }

    public void setIv_left(int resId) {
        iv_left.setImageResource(resId);
    }

    public void setIv_right(int resId) {
        iv_right.setImageResource(resId);
    }

    private ClickCallBack clickCallBack;

    public void setClickListener(ClickCallBack callBack) {
        clickCallBack = callBack;
    }

    public static interface ClickCallBack {

        void onLeftClick();

        void onRightClick();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_title:
                break;
            case R.id.iv_left:
                clickCallBack.onLeftClick();
                break;
            case R.id.iv_right:
                clickCallBack.onRightClick();
                break;
        }
    }
}
