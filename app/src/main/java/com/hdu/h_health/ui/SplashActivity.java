package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hdu.h_health.MyApplication;
import com.hdu.h_health.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 启动页
 * Created by See on 2016/2/25.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre);
        final Intent intent = new Intent();
        Timer timer = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                finish();
                intent.setClass(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(intent);
            }
        };
        timer.schedule(tt, 2000);
    }
}
