package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.ApplicationChart;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.widget.NavigationView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

/**
 * 咨询师入驻界面
 * Created by 612 on 2016/3/11.
 */
public class CounselorRegisterActivity extends BaseActivity {

    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.rbtn_shehui)
    RadioButton rbtnShehui;
    @Bind(R.id.rbtn_gaoxiao)
    RadioButton rbtnGaoxiao;
    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.tv_sex)
    TextView tvSex;
    @Bind(R.id.et_sex)
    EditText etSex;
    @Bind(R.id.tv_city)
    TextView tvCity;
    @Bind(R.id.et_city)
    EditText etCity;
    @Bind(R.id.tv_address)
    TextView tvAddress;
    @Bind(R.id.et_address)
    EditText etAddress;
    @Bind(R.id.tv_phone)
    TextView tvPhone;
    @Bind(R.id.et_phone)
    EditText etPhone;
    @Bind(R.id.tv_email)
    TextView tvEmail;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.btn_put_in)
    Button btnPutIn;
    @Bind(R.id.tv_type)
    TextView tvType;
    @Bind(R.id.tv_Iam)
    TextView tvIam;
    @Bind(R.id.ll_name)
    LinearLayout llName;
    @Bind(R.id.ll_sex)
    LinearLayout llSex;
    @Bind(R.id.ll_city)
    LinearLayout llCity;
    @Bind(R.id.ll_address)
    LinearLayout llAddress;
    @Bind(R.id.ll_phone)
    LinearLayout llPhone;
    @Bind(R.id.ll_email)
    LinearLayout llEmail;
    @Bind(R.id.iv_icon)
    ImageView ivIcon;
    @Bind(R.id.tv_base)
    TextView tvBase;

    private int applicationType;//申请类型
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_couregister_one);
        ButterKnife.bind(this);
        check();
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        user = BmobUser.getCurrentUser(this, User.class);
        navView.setTv_title("咨询师申请");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {

            }
        });
    }

    @OnClick(R.id.btn_put_in)
    public void onClick() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == R.id.rbtn_shehui) {
                    applicationType = 0;
                } else if (group.getCheckedRadioButtonId() == R.id.rbtn_gaoxiao) {
                    applicationType = 1;
                }
            }
        });
        if ((applicationType + "").isEmpty()) {
            toast("不能为空");
            return;
        }
        String name = etName.getText().toString();
        String sex = etSex.getText().toString();
        String city = etCity.getText().toString();
        String address = etAddress.getText().toString();
        String phone = etPhone.getText().toString();
        String email = etEmail.getText().toString();
        if (name.isEmpty() || sex.isEmpty() || city.isEmpty() || address.isEmpty() || phone.isEmpty() || email.isEmpty()) {
            toast("不能为空");
            return;
        }
        saveApplicationChart(user, applicationType, name, sex, city, address, phone, email);
    }

    //提交申请表,并建立申请用户与申请表的联系
    private void saveApplicationChart(User user, int applicationType, String name, String sex, String city, String address,
                                      String phone, String email) {
        ApplicationChart chart = new ApplicationChart();
        chart.setUser(user);//将用户和申请表关联起来
        chart.setAppliType(applicationType);//申请类型
        chart.setName(name);
        chart.setSex(sex);
        chart.setCity(city);
        chart.setAddress(address);
        chart.setPhone(phone);
        chart.setEmail(email);
        chart.setType(0);//申请表状态为待审核
        chart.save(this, new SaveListener() {
            @Override
            public void onSuccess() {
                toast("您的申请已提交成功！");
                if (tvType.VISIBLE == View.GONE) {
                    tvType.setVisibility(View.VISIBLE);
                    tvType.setText("待审核");
                    ivIcon.setVisibility(View.VISIBLE);
                }
                hideAllView();
                tvType.setVisibility(View.VISIBLE);
                ivIcon.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int i, String s) {
                toast("提交失败！");
            }
        });
    }

    //检查是否有以前有申请过，一个账号只能申请一次
    private void check() {
        BmobQuery<ApplicationChart> query = new BmobQuery<ApplicationChart>();
        query.addWhereEqualTo("user", user);
        query.order("-updatedAt");
        query.include("user");
        query.findObjects(this, new FindListener<ApplicationChart>() {
            @Override
            public void onSuccess(List<ApplicationChart> list) {
                if (list.size() >= 0 && list.get(0).getType() == 0) {
                    hideAllView();
                    tvType.setVisibility(View.VISIBLE);
                    toast("你的申请正在审核！请耐心等待...");
                    tvType.setText("待审核");
                    ivIcon.setVisibility(View.VISIBLE);
                } else if (list.size() >= 0 && list.get(0).getType() == 1) {
                    hideAllView();
                    tvType.setVisibility(View.VISIBLE);
                    tvType.setText("审核已通过");
                    ivIcon.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(int i, String s) {
                toast("查询失败！" + i + s);
            }
        });
    }

    //如果是已申请，则隐藏申请栏
    private void hideAllView() {
        tvIam.setVisibility(View.GONE);
        radioGroup.setVisibility(View.GONE);
        llName.setVisibility(View.GONE);
        llSex.setVisibility(View.GONE);
        llAddress.setVisibility(View.GONE);
        llCity.setVisibility(View.GONE);
        llEmail.setVisibility(View.GONE);
        llPhone.setVisibility(View.GONE);
        btnPutIn.setVisibility(View.GONE);
        tvBase.setVisibility(View.GONE);
//        etName.setVisibility(View.GONE);
//        tvName.setVisibility(View.GONE);
//        etSex.setVisibility(View.GONE);
//        tvSex.setVisibility(View.GONE);
//        etCity.setVisibility(View.GONE);
//        tvCity.setVisibility(View.GONE);
//        etAddress.setVisibility(View.GONE);
//        tvAddress.setVisibility(View.GONE);
//        etPhone.setVisibility(View.GONE);
//        tvPhone.setVisibility(View.GONE);
//        etEmail.setVisibility(View.GONE);
//        tvEmail.setVisibility(View.GONE);
//        radioGroup.setVisibility(View.GONE);

    }
}
