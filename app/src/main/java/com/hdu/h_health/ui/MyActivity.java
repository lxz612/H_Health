package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.utils.ViewUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * "我的"详情页--此页引入注解框架butterknife
 * Created by 612 on 2016/3/8.
 */
public class MyActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.iv_avatar)
    ImageView avatar;
    @Bind(R.id.nickname)
    TextView nickname;
    @Bind(R.id.username)
    TextView username;
    @Bind(R.id.head_arrow)
    ImageView headArrow;
    @Bind(R.id.head)
    RelativeLayout head;
    @Bind(R.id.heart)
    LinearLayout heart;
    @Bind(R.id.btn_consult)
    LinearLayout consult;
    @Bind(R.id.register)
    LinearLayout register;
    @Bind(R.id.setting)
    LinearLayout setting;
    @Bind(R.id.feedback)
    LinearLayout feedback;
    @Bind(R.id.tv_Login)
    TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        setMyInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMyInfo();
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        navView.setTv_title("我的");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {

            }
        });
    }

    //检查是否登录并设置当前用户信息
    private void setMyInfo() {
        boolean isLogin=UserModel.getInstance().checkIsLogin(this);
        if (!isLogin){
            avatar.setImageResource(R.mipmap.head);
            tvLogin.setVisibility(View.VISIBLE);
            username.setVisibility(View.GONE);
            nickname.setVisibility(View.GONE);
            return;
        }
        tvLogin.setVisibility(View.GONE);
        username.setVisibility(View.VISIBLE);
        nickname.setVisibility(View.VISIBLE);
        User user = UserModel.getInstance().getCurrentUser();
        String currentAvatar = user.getAvatar();
        String currentUsername = user.getUsername();
        String currentNick = user.getNick();
        ViewUtils.setPic(currentAvatar, R.mipmap.head, avatar);
        username.setText(TextUtils.isEmpty(currentUsername) ? "" : currentUsername);
        nickname.setText(TextUtils.isEmpty(currentNick) ? "" : currentNick);
    }

    @OnClick({R.id.head, R.id.heart,R.id.order, R.id.btn_consult, R.id.register, R.id.setting, R.id.feedback})
    public void onClick(View view) {
        boolean isLogin=UserModel.getInstance().checkIsLogin(this);
        switch (view.getId()) {
            case R.id.head:
                if (!isLogin){
                    CommonUtils.showToast(this, "请先登录！");
                    startActivity(new Intent(this, LoginActivity.class));
                    return;
                }
                startActivity(new Intent(this, SetMyInfoActivity.class));
                break;
            case R.id.heart:
                if (!isLogin){
                    CommonUtils.showToast(this, "请先登录！");
                    startActivity(new Intent(this, LoginActivity.class));
                    return;
                }
                startActivity(MySecretActivity.class, null, false);
                break;
            case R.id.order:
                if (!isLogin){
                    CommonUtils.showToast(this, "请先登录！");
                    startActivity(new Intent(this, LoginActivity.class));
                    return;
                }
                startActivity(MyOrderActivity.class, null, false);
                break;
            case R.id.btn_consult:
                if (!isLogin){
                    CommonUtils.showToast(this, "请先登录！");
                    startActivity(new Intent(this, LoginActivity.class));
                    return;
                }
                startActivity(new Intent(this, MyConsultActivity.class));
                break;
            case R.id.register:
                if (!isLogin){
                    CommonUtils.showToast(this, "请先登录！");
                    startActivity(new Intent(this, LoginActivity.class));
                    return;
                }
                startActivity(CounselorRegisterActivity.class, null, false);
                break;
            case R.id.setting:
                startActivity(SettingActivity.class, null, false);
                break;
            case R.id.feedback:
                break;
        }
    }
}
