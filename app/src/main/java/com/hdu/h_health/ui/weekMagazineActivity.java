package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 心理周刊
 * Created by 612 on 2016/2/27.
 */
public class weekMagazineActivity extends BaseActivity {

    @Bind(R.id.navView)
    NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekmagazine);
        ButterKnife.bind(this);

        navView.setTv_title("心理周刊");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
//                CommonUtils.showToast(getBaseContext(), "left");
            }

            @Override
            public void onRightClick() {
//                startActivity(new Intent(getBaseContext(), EditSecretActivity.class));
            }
        });
    }
}
