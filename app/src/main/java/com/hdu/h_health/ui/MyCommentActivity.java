package com.hdu.h_health.ui;

import android.os.Bundle;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;

/**
 * 我的评论界面
 * Created by 612 on 2016/4/4.
 */
public class MyCommentActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycomment);
    }

    @Override
    protected void initView() {
        super.initView();
    }
}
