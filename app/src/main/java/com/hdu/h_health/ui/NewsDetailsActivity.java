package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.widget.NavigationView;

/**
 * 资讯详情页
 * Created by 612 on 2016/2/27.
 */
public class NewsDetailsActivity extends BaseActivity {

    public static final String URL = "url";

    private WebView mWebView;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsbrowers);
    }

    @Override
    protected void initView() {
        super.initView();
        mWebView = (WebView) findViewById(R.id.webview);
        String url = getIntent().getStringExtra(NewsDetailsActivity.URL);
        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值为true时，在Webview中打开，为False时，在系统浏览器打开
                view.loadUrl(url);
                return true;
            }
        });
        //支持javaScript
        mWebView.getSettings().setJavaScriptEnabled(true);
        //webView加载优先使用缓存
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    //网页加载完毕,关闭ProgressDialog
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    //加载时，打开ProgressDialog
                    mProgressBar.setVisibility(View.VISIBLE);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setTv_title("文章详情");
        navigationView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navigationView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
            }
        });
    }

    /**
     * 启用物理返回按键
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finish() {
        super.finish();
    }
}
