package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.hdu.h_health.Adapter.MySecretAdapter;
import com.hdu.h_health.R;
import com.hdu.h_health.bean.Secret;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.dependence.xlistview.XListView;
import com.hdu.h_health.model.SecretModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.FindListener;

/**
 * 我的心事
 * Created by 612 on 2016/3/23.
 */
public class MySecretActivity extends BaseActivity implements XListView.IXListViewListener, AdapterView.OnItemClickListener {
    private XListView xListView;
    private List<Secret> secrets = new ArrayList<Secret>();
    private MySecretAdapter adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mysecret);
        adapter = new MySecretAdapter(this, secrets);
        xListView.setAdapter(adapter);
        xListView.setXListViewListener(this);
        xListView.setOnItemClickListener(this);
        xListView.setPullLoadEnable(true);//设置上拉加载
        query(0);


        navigationView.setTv_title("我的心事");
        navigationView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
        navigationView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
                startActivity(new Intent(getBaseContext(), EditSecretActivity.class));
            }
        });
    }

    @Override
    protected void initView() {
        super.initView();
        xListView = (XListView) findViewById(R.id.listView);
        progressBar= (ProgressBar) findViewById(R.id.progressbar);
        navigationView= (NavigationView) findViewById(R.id.navView);
    }

    private void query(final int page) {
        User user = BmobUser.getCurrentUser(this, User.class);
        SecretModel.getInstance().getSecret(this, page, user, new FindListener<Secret>() {
            @Override
            public void onSuccess(List<Secret> list) {
                CommonUtils.showLog("查询成功");
                if (list.size() >= 0) {
                    if (page==0&&secrets.size() > 0) {
                        secrets.clear();
                    }
                    for (Secret se : list) {
                        secrets.add(se);
                    }
                } else {
                    CommonUtils.showToast(getBaseContext(), "没有数据");
                }
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(int i, String s) {
                CommonUtils.showLog("查询失败");
                CommonUtils.showToast(getBaseContext(), "查询失败");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private int curPage = 0;// 当前页的编号，从0开始

    //下拉加载
    @Override
    public void onRefresh() {
        curPage = 0;
        query(curPage);
        stopLoad();
    }

    //上拉加载
    @Override
    public void onLoadMore() {
        curPage++;
        query(curPage);
        stopLoad();
    }

    //停止下拉刷新和上拉加载
    private void stopLoad() {
        xListView.stopRefresh();
        xListView.stopLoadMore();
        xListView.setRefreshTime(R.string.xlistview_time);//设置刷新时间
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Secret secret = secrets.get(position - 1);
        Intent intent = new Intent(this, CommentActivity.class);
        intent.putExtra("secret", secret);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        secrets.clear();
    }
}
