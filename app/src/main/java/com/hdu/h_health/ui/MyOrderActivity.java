package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.hdu.h_health.Adapter.MyOrderAdapter;
import com.hdu.h_health.Adapter.MySecretAdapter;
import com.hdu.h_health.R;
import com.hdu.h_health.bean.Order;
import com.hdu.h_health.bean.Secret;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.dependence.xlistview.XListView;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.FindListener;

/**
 * 我的订单
 * Created by 612 on 2016/4/4.
 */
public class MyOrderActivity extends BaseActivity implements XListView.IXListViewListener, AdapterView.OnItemClickListener{

    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.progressbar)
    ProgressBar progressbar;
    @Bind(R.id.listView)
    XListView listView;
    private List<Order> orders = new ArrayList<Order>();
    private MyOrderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorder);
        adapter = new MyOrderAdapter(this,orders);
        listView.setAdapter(adapter);
        listView.setXListViewListener(this);
        listView.setOnItemClickListener(this);
        listView.setPullLoadEnable(true);//设置上拉加载
        queryOrder();
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        navView.setTv_title("我的订单");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
            }
        });
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    //查询订单
    private void queryOrder() {
        BmobQuery<Order> query = new BmobQuery<Order>();
        query.setLimit(5);
        query.addWhereEqualTo("author",BmobUser.getCurrentUser(this, User.class));
        query.include("counsult");
        query.findObjects(this, new FindListener<Order>() {
            @Override
            public void onSuccess(List<Order> list) {
                if (list.size()>0){
                    for (Order order:list){
                        orders.add(order);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onError(int i, String s) {
                CommonUtils.showToast(getBaseContext(),i+s);
            }
        });
    }
}
