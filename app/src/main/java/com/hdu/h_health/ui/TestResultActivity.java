package com.hdu.h_health.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;

/**
 * Created by See on 2016/3/26.
 * 测试结果界面
 */
public class TestResultActivity extends BaseActivity{
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testresult);
        textView = (TextView) findViewById(R.id.testResult_textview);
        textView.setText("A");
    }
}
