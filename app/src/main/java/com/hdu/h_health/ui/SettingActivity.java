package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 系统设置区
 * Created by 612 on 2016/3/11.
 */
public class SettingActivity extends BaseActivity {
    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.rl_share)
    RelativeLayout rlShare;
    @Bind(R.id.rl_clean)
    RelativeLayout rlClean;
    @Bind(R.id.rl_check)
    RelativeLayout rlCheck;
    @Bind(R.id.rl_about)
    RelativeLayout rlAbout;
    @Bind(R.id.btn_logout)
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        navView.setTv_title("设置");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {

            }
        });
    }

    @OnClick({R.id.rl_share, R.id.rl_clean, R.id.rl_check, R.id.rl_about, R.id.btn_logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_share:
                CommonUtils.showToast(this,"分享成功！");
                break;
            case R.id.rl_clean:
                CommonUtils.showToast(this,"缓存清理成功！");
                break;
            case R.id.rl_check:
                CommonUtils.showToast(this,"已是最新版本！");
                break;
            case R.id.rl_about:
                break;
            case R.id.btn_logout:
                if (UserModel.getInstance().checkIsLogin(this)){
                    UserModel.getInstance().logout();
                    finish();
                }else {

                }
                break;
        }
    }
}
