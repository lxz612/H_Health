package com.hdu.h_health.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

/**
 * 心理测试
 */

public class ConsultTestActivity extends BaseActivity{
//    private Button testButton;
//    //点赞
//    private CheckBox ZanCheckbox;
    private WebView webView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_test);
//        progressBar = (ProgressBar) findViewById(R.id.consult_test_progressBar);
        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setTv_title("新健康");
        navigationView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
            }
        });

        webView = (WebView)findViewById(R.id.consult_test_webView);

        //得到webview设置
        final WebSettings webSettings = webView.getSettings();

        //允许使用JavaScript
        webSettings.setJavaScriptEnabled(true);
        //提高网页加载速度，暂时阻塞图片加载，然后网页加载好了，在进行加载图片
        webSettings.setBlockNetworkImage(true);
        //开启缓存机制
        webSettings.setAppCacheEnabled(true);
        //外部心理测试链接
        webView.loadUrl("http://m.weiceyan.com/#modile.qq.com");

        //使外链不跳转到外部浏览器
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if(progressDialog == null) {
                    progressDialog = new ProgressDialog(ConsultTestActivity.this);
                    progressDialog.setMessage("加载中...");
                    progressDialog.show();
                    webView.setEnabled(false);
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                    webView.setEnabled(true);
                }
                webSettings.setBlockNetworkImage(false);
                super.onPageFinished(view, url);
            }

//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl("http://m.weiceyan.com/s/87.htm");
//                return true;
//            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });

//        testButton = (Button) findViewById(R.id.consult_test_testButton);
//        ZanCheckbox = (CheckBox) findViewById(R.id.test_dianzan);
//        testButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setClass(ConsultTestActivity.this, ToTestActivity.class);
//                startActivity(intent);
//            }
//        });
    }
    public boolean onKeyDown(int keyCoder,KeyEvent event){
        if(webView.canGoBack() && keyCoder == KeyEvent.KEYCODE_BACK){
            webView.goBack();   //goBack()表示返回webView的上一页面
            return true;
        }
        return super.onKeyDown(keyCoder, event);

    }
}
