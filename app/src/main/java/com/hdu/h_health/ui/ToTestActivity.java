package com.hdu.h_health.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.hdu.h_health.R;
import com.hdu.h_health.Adapter.ToTestAdapter;
import com.hdu.h_health.bean.ToTestBean;
import com.hdu.h_health.common.BaseActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.GetListener;
import cn.bmob.v3.listener.SaveListener;

/**
 *测试
 */
public class ToTestActivity extends BaseActivity{
    private ListView listView;
    Button submitButton;
    ToTestBean toTestBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totest);
        Bmob.initialize(this, "4edd9cd1cd7be7cc9b6c73511aeea396");
        final List<BmobObject> toTestBeans = new ArrayList<BmobObject>();
        final ToTestBean toTestBean = new ToTestBean();
        BmobQuery<ToTestBean> query = new BmobQuery<>();
        query.addWhereEqualTo("tablename", "tab");
        query.findObjects(this, new FindListener<ToTestBean>() {
            @Override
            public void onSuccess(List<ToTestBean> list) {
                toast("Success");
                for(ToTestBean bean: list) {
                    bean.getQuestion();
                    bean.getOption1();
                    bean.getOption2();
                    bean.getOption3();

                }
            }

            @Override
            public void onError(int i, String s) {
                toast("Error");
            }
        });

        listView = (ListView) findViewById(R.id.totest_listview);
        submitButton = (Button) findViewById(R.id.totest_submit_button);
//        List<BmobObject> toTestBeans = new ArrayList<>();
//        for(int i = 0; i < 10; i++) {
//            toTestBeans.add(new ToTestBean("Question" + i,
//                    "选项A", "选项B", "选项C"));
//        }
        listView.setAdapter(new ToTestAdapter(this, toTestBeans));

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ToTestActivity.this, TestResultActivity.class);
                Bundle bundle = new Bundle();
                String[] str = new String[10];
                str[0] = String.valueOf(ToTestAdapter.countA);
                str[1] = String.valueOf(ToTestAdapter.countB);
                str[2] = String.valueOf(ToTestAdapter.countC);
                bundle.putStringArrayList("strKey", (ArrayList<String>) Arrays.asList(str));
                startActivity(intent);
            }
        });
    }

    public void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
