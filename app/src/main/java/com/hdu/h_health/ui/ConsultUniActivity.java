package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.hdu.h_health.Adapter.ConsultZXAdapter;
import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.dependence.xlistview.XListView;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.listener.FindListener;

/**
 * 高校入口
 */
public class ConsultUniActivity extends BaseActivity implements AdapterView.OnItemClickListener, XListView.IXListViewListener {
    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.listview)
    XListView listview;
    @Bind(R.id.progressbar)
    ProgressBar progressbar;
    private List<User> users = new ArrayList<User>();
    private ConsultZXAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_uni);
        adapter = new ConsultZXAdapter(this, users);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(this);
        listview.setXListViewListener(this);
        listview.setPullLoadEnable(true);
        query(0, STATE_REFRESH);

    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        navView.setTv_title("高校咨询师");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_perm_identity_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
            }
        });
    }

    private int curPage = 0;

    @Override
    public void onRefresh() {
        curPage = 0;
        query(curPage, STATE_REFRESH);
        stopLoad();
    }

    @Override
    public void onLoadMore() {
        curPage++;
        query(curPage, STATE_MORE);
        stopLoad();
    }

    //停止下拉刷新和上拉加载
    private void stopLoad() {
        listview.stopRefresh();
        listview.stopLoadMore();
        listview.setRefreshTime(R.string.xlistview_time);//设置刷新时间
    }

    private static final int STATE_REFRESH = 0;// 下拉刷新
    private static final int STATE_MORE = 1;// 加载更多

    //查询咨询师列表数据
    private void query(int page, final int type) {
        UserModel.getInstance().queryUserByType(this,1, page, new FindListener<User>() {
            @Override
            public void onSuccess(List<User> list) {
                if (list.size() >= 0) {
                    if (type == STATE_REFRESH) {
                        users.clear();
                    }
                    for (User td : list) {
                        users.add(td);
                    }
                } else if (type == STATE_MORE) {
                    CommonUtils.showToast(getBaseContext(), "没有更多咨询师");
                } else if (type == STATE_REFRESH) {
                    CommonUtils.showToast(getBaseContext(), "暂无咨询师");
                }
                adapter.notifyDataSetChanged();
                progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onError(int i, String s) {
                toast("查询出错" + i + " " + s);
                progressbar.setVisibility(View.GONE);
            }
        });
    }

    //列表项点击事件
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = users.get(position - 1);
        Bundle bundle = new Bundle();
        bundle.putString("User", user.getObjectId());
        startActivity(PersonalInfoActivity.class, bundle, false);
    }
}
