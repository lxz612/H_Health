package com.hdu.h_health.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.model.SecretModel;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import cn.bmob.v3.listener.SaveListener;

/**
 * 编辑心事界面
 * Created by 612 on 2016/3/22.
 */
public class EditSecretActivity extends BaseActivity implements View.OnClickListener{

    private EditText edit_content;
    private Button publish;
    private LinearLayout open_layout,take_layout;
    private ImageView open_pic,take_pic;
    private String dateTime;

    protected NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editsecret);
        publish.setOnClickListener(this);
    }

    @Override
    protected void initView() {
        super.initView();
        edit_content= (EditText) findViewById(R.id.edit_content);
        publish= (Button) findViewById(R.id.btn_publish);
        open_layout= (LinearLayout) findViewById(R.id.open_layout);
        take_layout= (LinearLayout) findViewById(R.id.take_layout);

        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setTv_title("心事求助");
        navigationView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {

            }
        });

    }

    //发表心事
    private void publishSecret(){
        String content=edit_content.getText().toString();
        SecretModel.getInstance().publishSecret(this, content, new SaveListener() {
            @Override
            public void onSuccess() {
                CommonUtils.showToast(getBaseContext(), "发表成功！");
                CommonUtils.showLog("发表成功");
                finish();
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(getBaseContext(), "发表失败！");
                CommonUtils.showLog("发表失败");
            }
        });
        edit_content.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_publish:
                publishSecret();
                break;
        }
    }
}
