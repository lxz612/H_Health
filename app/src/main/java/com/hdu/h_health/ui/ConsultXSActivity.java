package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.fragments.PrivateFragment;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

/**
 * 心事模块
 */
public class ConsultXSActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_xinshi);
        PrivateFragment fragment = new PrivateFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.xinshifragment, fragment);
        transaction.commit();

        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setTv_title("新健康");
        navigationView.getIv_left().setImageResource(R.drawable.ic_search_white_36dp);
        navigationView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                CommonUtils.showToast(getBaseContext(), "left");
            }

            @Override
            public void onRightClick() {
                startActivity(new Intent(getBaseContext(), EditSecretActivity.class));
            }
        });
    }

    @Override
    protected void initView() {
        super.initView();
    }
}
