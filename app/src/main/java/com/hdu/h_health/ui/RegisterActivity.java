package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.hdu.h_health.R;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.event.FinishEvent;
import com.hdu.h_health.model.BaseModel;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import de.greenrobot.event.EventBus;

/**
 * 用户注册界面
 * Created by 612 on 2016/2/27.
 */
public class RegisterActivity extends BaseActivity implements OnClickListener {

    @Bind(R.id.navView)
    NavigationView navView;
    private EditText username, password, password_again;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        navView.setTv_title("注册");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {

            }
        });
        register.setOnClickListener(this);

    }

    private void init() {
        ButterKnife.bind(this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        password_again = (EditText) findViewById(R.id.password_again);
        register = (Button) findViewById(R.id.register_btn);

    }

    @Override
    public void onClick(View v) {
        UserModel.getInstance().register(this, username.getText().toString(), password.getText().toString(), password_again.getText().toString(), new LogInListener() {
            @Override
            public void done(Object o, BmobException e) {
                if (e == null) {//登录没有错误
                    EventBus.getDefault().post(new FinishEvent());//发送“结束事件”
                    finish();
//                    startActivity(MainActivity.class, null,true);
                } else {
                    if (e.getErrorCode() == BaseModel.CODE_NOT_EQUAL) {
                        password_again.setText("");
                    }
                    toast(e.getMessage() + "(" + e.getErrorCode() + ")");
                }
            }
        });
    }
}
