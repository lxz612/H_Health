package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.hdu.h_health.Adapter.ConsultZXAdapter;
import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.dependence.xlistview.XListView;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.FindListener;

//咨询预约界面
public class ConsultZXActivity extends BaseActivity implements AdapterView.OnItemClickListener, XListView.IXListViewListener {

    private XListView xListView;
    private ConsultZXAdapter adapter;
    private List<User> users = new ArrayList<User>();
    private ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_zixun);
        initView();
        adapter = new ConsultZXAdapter(this, users);
        xListView.setAdapter(adapter);
        xListView.setOnItemClickListener(this);
        xListView.setXListViewListener(this);
        xListView.setPullLoadEnable(true);
        query(0, STATE_REFRESH);


    }

    @Override
    protected void initView() {
        super.initView();
        xListView = (XListView) findViewById(R.id.listview);
        progressbar= (ProgressBar) findViewById(R.id.progressbar);

        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.setTv_title("咨询师");
        navigationView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navigationView.getIv_right().setImageResource(R.drawable.ic_perm_identity_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
                //检查是否登录
//                boolean isLogin = UserModel.getInstance().checkIsLogin(getBaseContext());
//                if (!isLogin) {
//                    CommonUtils.showToast(getBaseContext(), "请先登录！");
//                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
//                    return;
//                }
//                startActivity(new Intent(getBaseContext(), MyActivity.class));
            }
        });
    }

    private int curPage = 0;

    @Override
    public void onRefresh() {
        curPage = 0;
        query(curPage, STATE_REFRESH);
        stopLoad();
    }

    @Override
    public void onLoadMore() {
        curPage++;
        query(curPage, STATE_MORE);
        stopLoad();
    }

    //停止下拉刷新和上拉加载
    private void stopLoad() {
        xListView.stopRefresh();
        xListView.stopLoadMore();
        xListView.setRefreshTime(R.string.xlistview_time);//设置刷新时间
    }

    private static final int STATE_REFRESH = 0;// 下拉刷新
    private static final int STATE_MORE = 1;// 加载更多

    //查询咨询师列表数据
    private void query(int page, final int type) {
        UserModel.getInstance().queryUserByType(this, 3, page, new FindListener<User>() {
            @Override
            public void onSuccess(List<User> list) {
                if (list.size() >= 0) {
                            if (type == STATE_REFRESH) {
                                users.clear();
                            }
                            for (User td : list) {
                                users.add(td);
                    }
                } else if (type == STATE_MORE) {
                    CommonUtils.showToast(getBaseContext(), "没有更多咨询师");
                } else if (type == STATE_REFRESH) {
                    CommonUtils.showToast(getBaseContext(), "暂无咨询师");
                }
                adapter.notifyDataSetChanged();
                progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onError(int i, String s) {
                toast("查询出错" + i + " " + s);
                progressbar.setVisibility(View.GONE);
            }
        });
    }

    //列表项点击事件
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = users.get(position - 1);
        Bundle bundle = new Bundle();
        bundle.putString("User", user.getObjectId());
        startActivity(PersonalInfoActivity.class, bundle, false);
    }
}
