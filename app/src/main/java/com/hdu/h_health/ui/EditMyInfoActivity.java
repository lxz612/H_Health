package com.hdu.h_health.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.UpdateListener;

/**
 * 更改个人信息页
 * Created by 612 on 2016/3/19.
 */
public class EditMyInfoActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.btn_store)
    Button btnStore;
    @Bind(R.id.et_nickname)
    EditText etNickname;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.et_sex)
    EditText etSex;
    @Bind(R.id.et_summary)
    EditText etSummary;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editmyinfo);
        type = getIntent().getIntExtra("type", 0);
        //设置布局显示
        hideAllView();
        switch (type) {
            case 0:
                navView.setTv_title("更改昵称");
                etNickname.setVisibility(View.VISIBLE);
                break;
            case 1:
                navView.setTv_title("更改邮箱");
                etEmail.setVisibility(View.VISIBLE);
                break;
            case 2:
                navView.setTv_title("更改性别");
                etSex.setVisibility(View.VISIBLE);
                break;
            case 3:
                navView.setTv_title("更改签名");
                etSummary.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        btnStore.setOnClickListener(this);
        navView.setTv_title("新健康");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
//                startActivity(new Intent(getBaseContext(), EditSecretActivity.class));
            }
        });
    }

    private void hideAllView() {
        etNickname.setVisibility(View.GONE);
        etEmail.setVisibility(View.GONE);
        etSex.setVisibility(View.GONE);
        etSummary.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        User user = new User();
        switch (type) {
            case 0:
                String nickname = etNickname.getText().toString();
                if (nickname.isEmpty()) {
                    CommonUtils.showToast(this, "昵称不能为空");
                    return;
                }
                user.setNick(nickname);
                break;
            case 1:
                String email = etEmail.getText().toString();
                if (email.isEmpty()) {
                    CommonUtils.showToast(this, "邮箱不能为空");
                    return;
                }
                user.setEmail(email);
                break;
            case 2:
                String sex = etSex.getText().toString();
                if (sex.isEmpty()) {
                    CommonUtils.showToast(this, "性别不能为空");
                    return;
                }
                if (sex.equals("男")) {
                    user.setSex(true);
                } else if (sex.equals("女")) {
                    user.setSex(false);
                } else {
                    CommonUtils.showToast(this, "只能填男或女");
                    user.setSex(true);
                }
                break;
            case 3:
                String summary = etSummary.getText().toString();
                if (summary.isEmpty()) {
                    CommonUtils.showToast(this, "签名不能为空");
                    return;
                }
                user.setSummary(summary);
                break;
            default:
                break;
        }
        BmobUser bmobUser = BmobUser.getCurrentUser(this, User.class);
        user.update(this, bmobUser.getObjectId(), new UpdateListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getBaseContext(), "更新用户信息成功", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int i, String s) {
                Toast.makeText(getBaseContext(), "更新用户信息失败", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
