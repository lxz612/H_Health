package com.hdu.h_health.ui;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.Order;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.utils.ViewUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 付款界面
 * Created by 612 on 2016/4/5.
 */
public class PayOrderActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.tv_order_number)
    TextView tvOrderNumber;
    @Bind(R.id.iv_avatar)
    ImageView ivAvatar;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.tv_type)
    TextView tvType;
    @Bind(R.id.tv_number)
    TextView tvNumber;
    @Bind(R.id.tv_acount)
    TextView tvAcount;
    @Bind(R.id.rb_alipay)
    RadioButton rbAlipay;
    @Bind(R.id.rb_weixinpay)
    RadioButton rbWeixinpay;
    @Bind(R.id.iv_icon)
    ImageView ivIcon;
    @Bind(R.id.tv_type_second)
    TextView tvTypeSecond;
    @Bind(R.id.tv_acount_second)
    TextView tvAcountSecond;
    @Bind(R.id.btn_pay)
    Button btnPay;
    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;

    private Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payorder);
        navView.setTv_title("支付");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
//                startActivity(new Intent(getBaseContext(), MyActivity.class));
            }
        });
        radioGroup.setOnCheckedChangeListener(this);
        setOrderInfo();
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        order= (Order) getBundle().getSerializable("order");
    }

    //支付选择
    private void chooseRb(int id) {
        if (id == R.id.rb_alipay) {
            ivIcon.setImageResource(R.drawable.iconfont_rectangle390);
            tvTypeSecond.setText("支付宝");
        } else {
            ivIcon.setImageResource(R.drawable.iconfont_weixin);
            tvTypeSecond.setText("微信");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rb_weixinpay:
                chooseRb(R.id.rb_weixinpay);
                break;
            case R.id.rb_alipay:
                chooseRb(R.id.rb_alipay);
                break;
        }
    }

    private void setOrderInfo(){
        if (order!=null){
            ViewUtils.setPic(order.getCounsult().getAvatar(),R.mipmap.head,ivAvatar);
            //预约号暂时用ObjectId来替代
            tvOrderNumber.setText(order.getObjectId());
            tvName.setText(order.getCounsult().getNick());
            if (order.getConType()==0){
                tvType.setText("语音咨询");
            }else if(order.getConType()==1){
                tvType.setText("视频咨询");
            }else if (order.getConType()==2){
                tvType.setText("面对面咨询");
            }
            tvNumber.setText(order.getSum() + "次");
            tvAcount.setText("¥"+order.getAcount()+".0");
            tvAcountSecond.setText("共"+order.getAcount()+".00元");
        }
    }
}
