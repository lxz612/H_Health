package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hdu.h_health.MyApplication;
import com.hdu.h_health.R;
import com.hdu.h_health.fragments.ConsultFragment;
import com.hdu.h_health.fragments.NewsFragment;
import com.hdu.h_health.fragments.PrivateFragment;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    //定义3个Fragment的对象
    private ConsultFragment fg1;
    private NewsFragment fg2;
    private PrivateFragment fg3;

    //底部导航栏中的ImageView与TextView
    private ImageView course_image;
    private ImageView found_image;
    private ImageView settings_image;
    private TextView course_text;
    private TextView settings_text;
    private TextView found_text;

    //定义要用的颜色值
    private final static int GRAY = 0xFF7597B3;
    private final static int BLUE = 0xFF0AB2FB;

    //FragmentManager对象
    private FragmentManager fManager;

    private NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 我设置了两个动画资源文件作为启动动画和退出动画
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setChoiceItem(0);
//        MyApplication.getInstance().addActivity(this);
//        //监听连接状态，也可通过BmobIM.getInstance().getCurrentStatus()来获取当前的长连接状态
//        BmobIM.getInstance().setOnConnectStatusChangeListener(new ConnectStatusChangeListener() {
//            @Override
//            public void onChange(ConnectionStatus status) {
//                Toast.makeText(getBaseContext(), "" + status.getMsg(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    //完成组件的初始化
    public void initViews() {
        fManager = getSupportFragmentManager();
        navView = (NavigationView) findViewById(R.id.navView);

        course_image = (ImageView) findViewById(R.id.course_image);
        found_image = (ImageView) findViewById(R.id.found_image);
        settings_image = (ImageView) findViewById(R.id.setting_image);
        course_text = (TextView) findViewById(R.id.course_text);
        found_text = (TextView) findViewById(R.id.found_text);
        settings_text = (TextView) findViewById(R.id.setting_text);

        RelativeLayout course_layout = (RelativeLayout) findViewById(R.id.rl_yuyue);
        RelativeLayout found_layout = (RelativeLayout) findViewById(R.id.rl_zixun);
        RelativeLayout settings_layout = (RelativeLayout) findViewById(R.id.rl_xinshi);

        course_layout.setOnClickListener(this);
        found_layout.setOnClickListener(this);
        settings_layout.setOnClickListener(this);
    }

    //重写onSaveInstanceState()方法，在Activity被系统回收时不保存Activity中视图状态
    //以此解决Fragment重影问题
    //解决网址：http://wml.farbox.com/post/fragment-overlay-problem
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }

    //重写onClick事件
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_yuyue:
                setChoiceItem(0);
                break;
            case R.id.rl_zixun:
                setChoiceItem(1);
                break;
            case R.id.rl_xinshi:
                setChoiceItem(2);
                break;
            default:
                break;
        }
    }

    //定义一个选中一个item后的处理
    public void setChoiceItem(int index) {
        FragmentTransaction transaction = fManager.beginTransaction();
        //重置选项+隐藏所有Fragment
        hideFragments(transaction);
        clearChoice();
        switch (index) {
            case 0:
                navView.setTv_title(getResources().getString(R.string.zixun));
                navView.getIv_right().setImageResource(R.drawable.ic_search_white_36dp);
                navView.getIv_left().setImageResource(R.drawable.ic_perm_identity_white_36dp);
                navView.setClickListener(new NavigationView.ClickCallBack() {
                    @Override
                    public void onLeftClick() {
                        startActivity(new Intent(getBaseContext(), MyActivity.class));
                    }

                    @Override
                    public void onRightClick() {
                        CommonUtils.showToast(getBaseContext(), "Search");
                    }
                });
                if (fg1 == null) {// 如果fg1为空，则创建一个并添加到界面上
                    fg1 = new ConsultFragment();
                    transaction.add(R.id.content, fg1);
                } else {// 如果fg1不为空，则直接将它显示出来
                    transaction.show(fg1);
                }
                setTextAndImg(course_image, course_text, R.drawable.zixun_press, BLUE);
                break;
            case 1:
                navView.setTv_title("资讯");
                navView.getIv_right().setImageResource(R.drawable.ic_search_white_36dp);
                navView.getIv_left().setImageResource(R.drawable.ic_perm_identity_white_36dp);
                navView.setClickListener(new NavigationView.ClickCallBack() {
                    @Override
                    public void onLeftClick() {
                        startActivity(new Intent(getBaseContext(), MyActivity.class));
                    }

                    @Override
                    public void onRightClick() {
                        CommonUtils.showToast(getBaseContext(), "Search");
                    }
                });
                if (fg2 == null) {
                    fg2 = new NewsFragment();
                    transaction.add(R.id.content, fg2);
                } else {
                    transaction.show(fg2);
                }
                setTextAndImg(found_image, found_text, R.drawable.zixun1_press, BLUE);
                break;
            case 2:
                navView.setTv_title("心事");
                navView.getIv_left().setImageResource(R.drawable.ic_perm_identity_white_36dp);
                navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
                navView.setClickListener(new NavigationView.ClickCallBack() {
                    @Override
                    public void onLeftClick() {
                        startActivity(new Intent(getBaseContext(), MyActivity.class));
                    }

                    @Override
                    public void onRightClick() {
                        //检查是否登录
                        boolean isLogin = UserModel.getInstance().checkIsLogin(getApplicationContext());
                        if (!isLogin) {
                            CommonUtils.showToast(getBaseContext(), "请先登录！");
                            startActivity(new Intent(getBaseContext(), LoginActivity.class));
                            return;
                        }
                        startActivity(new Intent(getBaseContext(), EditSecretActivity.class));
                    }
                });
                if (fg3 == null) {
                    fg3 = new PrivateFragment();
                    transaction.add(R.id.content, fg3);
                } else {
                    transaction.show(fg3);
                }
                setTextAndImg(settings_image, settings_text, R.drawable.xinshi_press, BLUE);
                break;
        }
        transaction.commit();
    }

    //设置底部导航文字和图片
    private void setTextAndImg(ImageView iv, TextView tv, int imgId, int color) {
        iv.setImageResource(imgId);
        tv.setTextColor(color);
    }

    //隐藏所有的Fragment,避免fragment混乱
    private void hideFragments(FragmentTransaction transaction) {
        if (fg1 != null) {
            transaction.hide(fg1);
        }
        if (fg2 != null) {
            transaction.hide(fg2);
        }
        if (fg3 != null) {
            transaction.hide(fg3);
        }
    }

    //重置所有选项
    public void clearChoice() {
        course_image.setImageResource(R.drawable.zixun_normal);
        course_text.setTextColor(GRAY);
        found_image.setImageResource(R.drawable.zixun1_normal);
        found_text.setTextColor(GRAY);
        settings_image.setImageResource(R.drawable.xinshi_normal);
        settings_text.setTextColor(GRAY);
    }

    private static long firstTime;

    //连续按两次返回键就退出
    @Override
    public void onBackPressed() {
        if (firstTime + 2000 > System.currentTimeMillis()) {
            MyApplication.getInstance().exit();
            super.onBackPressed();
        } else {
            Toast.makeText(MainActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
        }
        firstTime = System.currentTimeMillis();
    }
}
