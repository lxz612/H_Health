package com.hdu.h_health.ui;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.WeeklyBean;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.utils.ViewUtils;
import com.hdu.h_health.widget.NavigationView;

/**
 * 点击周刊的item跳转进入该Activity
 * Created by See on 2016/4/6.
 */
public class WeeklyDetailsActivity extends BaseActivity {

    private NavigationView navigationView;
    private ImageView imageView;
    private TextView title;
    private TextView content;
    private ScrollView scrollView;

    private WeeklyBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_details);
        bean= (WeeklyBean) getIntent().getSerializableExtra("weeklyBean");
        setData();
    }

    @Override
    protected void initView() {
        super.initView();
        scrollView = (ScrollView) findViewById(R.id.weekly_details_scrollView);
        imageView = (ImageView) findViewById(R.id.weekly_details_img);
        title = (TextView) findViewById(R.id.weekly_details_title);
        content = (TextView) findViewById(R.id.weekly_details_content);
        navigationView = (NavigationView) findViewById(R.id.navView);
        navigationView.getIv_left().setImageResource(R.drawable.iconfont_back);
//        navigationView.getIv_right().setImageResource(R.drawable.ic_perm_identity_white_36dp);
        navigationView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
            }
        });
    }

    private void setData(){
        ViewUtils.setPic(bean.getPicUrl(),R.drawable.loading,imageView);
        title.setText(bean.getTitle());
    }
}
