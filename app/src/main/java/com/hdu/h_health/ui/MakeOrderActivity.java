package com.hdu.h_health.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.Order;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.utils.ViewUtils;
import com.hdu.h_health.widget.NavigationView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.listener.SaveListener;

/**
 * 生成订单页面
 * Created by 612 on 2016/4/3.
 */
public class MakeOrderActivity extends BaseActivity {

    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.img_avatar)
    ImageView imgAvatar;
    @Bind(R.id.tv_nickname)
    TextView tvNickname;
    @Bind(R.id.tv_yuyin)
    TextView tvYuyin;
    @Bind(R.id.rela_yuyin)
    RelativeLayout relaYuyin;
    @Bind(R.id.tv_shiping)
    TextView tvShiping;
    @Bind(R.id.rela_shiping)
    RelativeLayout relaShiping;
    @Bind(R.id.tv_face)
    TextView tvFace;
    @Bind(R.id.rela_face)
    RelativeLayout relaFace;
    @Bind(R.id.add)
    TextView add;
    @Bind(R.id.tv_number)
    TextView tvNumber;
    @Bind(R.id.jian)
    TextView jian;
    @Bind(R.id.tv_acount)
    TextView tvAcount;
    @Bind(R.id.et_call)
    EditText etCall;
    @Bind(R.id.et_age)
    EditText etAge;
    @Bind(R.id.rb_sex_man)
    RadioButton rbSexMan;
    @Bind(R.id.rb_sex_woman)
    RadioButton rbSexWoman;
    @Bind(R.id.et_phone)
    EditText etPhone;
    @Bind(R.id.et_ques)
    EditText etQues;
    @Bind(R.id.cb_agree)
    CheckBox cbAgree;
    @Bind(R.id.btn_take)
    Button btnTake;
    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;

    private Order order;
    private int type = 0;
    private int conType =0;
    private int acount=0;//总金额
//    private int sum=0;//次数
    int number = 1;//次数
    private String call;
    private String age;
    private boolean sex = true;
    private String phone;
    private String ques;
    private boolean isPass;

    private User consult;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makeorder);
        consult= (User) getIntent().getSerializableExtra("User");
        setInfo();
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        navView.setTv_title("预约");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_control_point_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
//                startActivity(new Intent(getBaseContext(), MyActivity.class));
            }
        });
        rbSexMan.setChecked(true);
        etAge.setInputType(EditorInfo.TYPE_CLASS_PHONE);
        cbAgree.setChecked(true);
        isPass();
    }

    //设置头像和称谓
    private void setInfo() {
        if (!consult.getAvatar().isEmpty()){
            ViewUtils.setPic(consult.getAvatar(),R.mipmap.head,imgAvatar);
            CommonUtils.showLog(consult.getAvatar());
        }
        tvNickname.setText(consult.getNick());
    }

    //添加订单
    private void addOrder() {
        if (acount==0){
            CommonUtils.showToast(this,"请选择服务！");
            return;
        }
        call = etCall.getText().toString();
        age = etCall.getText().toString();
        phone = etPhone.getText().toString();
        ques = etQues.getText().toString();
        if (call.isEmpty()||age.isEmpty()||phone.isEmpty()||ques.isEmpty()){
            CommonUtils.showToast(this,"请正确填写！");
            return;
        }
        selectSex();
//        isPass();
        if (!isPass){
            CommonUtils.showToast(this,"请同意协议！");
            return;
        }

        order = new Order();
        order.setOrderNumber("");
        order.setType(type);
        order.setConType(conType);
        order.setSum(number);//次数
        order.setAcount(acount);
        order.setCall(call);
        order.setAge(age);
        order.setSex(sex);
        order.setPhoneNumber(phone);
        order.setQueDescription(ques);
        order.setIsPass(isPass);

        order.setComment("");
        order.setStar(5);

        order.setAuthor(UserModel.getInstance().getCurrentUser());
        order.setCounsult(consult);
        bundle=new Bundle();
        bundle.putSerializable("order",order);
        order.save(this, new SaveListener() {
            @Override
            public void onSuccess() {
                startActivity(PayOrderActivity.class,bundle,false);
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(getBaseContext(), "生成订单失败");
            }
        });
    }

    @OnClick({R.id.rela_yuyin, R.id.rela_shiping, R.id.rela_face, R.id.add, R.id.jian,R.id.btn_take})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rela_yuyin:
                select(0);
                break;
            case R.id.rela_shiping:
                select(1);
                break;
            case R.id.rela_face:
                select(2);
                break;
            case R.id.add:
                number++;
                tvNumber.setText(number + "");
                calculateSum(com);
                break;
            case R.id.jian:
                if (number > 1) {
                    number--;
                    tvNumber.setText(number + "");
                }
                calculateSum(com);
                break;
            case R.id.btn_take:
                addOrder();
                break;
        }
    }

    //选择事件
    private void select(int i) {
        conType =i;
        relaYuyin.setBackgroundColor(Color.parseColor("#f0f0f0"));
        relaShiping.setBackgroundColor(Color.parseColor("#f0f0f0"));
        relaFace.setBackgroundColor(Color.parseColor("#f0f0f0"));
        switch (i) {
            case 0:
                relaYuyin.setBackgroundColor(Color.parseColor("#7CCD7C"));
                calculateSum(300);
                break;
            case 1:
                relaShiping.setBackgroundColor(Color.parseColor("#7CCD7C"));
                calculateSum(400);
                break;
            case 2:
                relaFace.setBackgroundColor(Color.parseColor("#7CCD7C"));
                calculateSum(500);
                break;
        }
    }

    //存储选择
    private int com = 0;

    //计算总金额
    private void calculateSum(int i) {
        com = i;
        acount = com * number;
        tvAcount.setText("共" + acount + ".0元");
    }

    //性别选择
    private void selectSex() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioButton = group.getCheckedRadioButtonId();
                switch (radioButton) {
                    case R.id.rb_sex_man:
                        sex = true;
                        break;
                    case R.id.rb_sex_woman:
                        sex = false;
                        break;
                }
            }
        });
    }

    //协议同意事件监听
    private void isPass() {
        isPass=cbAgree.isChecked();
        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPass = isChecked;
                if (isPass){
                    CommonUtils.showLog("true");
                }else {
                    CommonUtils.showLog("false");
                }
            }
        });
    }
}
