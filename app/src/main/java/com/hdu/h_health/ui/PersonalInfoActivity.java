package com.hdu.h_health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.BaseActivity;
import com.hdu.h_health.event.ChatEvent;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.model.i.QueryUserListener;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.utils.ViewUtils;
import com.hdu.h_health.widget.NavigationView;
import com.orhanobut.logger.Logger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.newim.BmobIM;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.listener.ConversationListener;
import cn.bmob.v3.exception.BmobException;
import de.greenrobot.event.EventBus;

/**
 * 咨询师、心理老师或用户主页
 * Created by 612 on 2016/3/8.
 */
public class PersonalInfoActivity extends BaseActivity {
    @Bind(R.id.navView)
    NavigationView navView;
    @Bind(R.id.iv_avatar)
    ImageView ivAvatar;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.tv_job)
    TextView tvJob;
    @Bind(R.id.tv_renzhen)
    TextView tvRenzhen;
    @Bind(R.id.tv_good)
    TextView tvGood;
    @Bind(R.id.btn_consult)
    RelativeLayout btnConsult;
    @Bind(R.id.btn_appointment)
    RelativeLayout btnAppointment;

    private User user;
    BmobIMUserInfo info;
    private String objectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalinfo);
        Bundle bundle = getBundle();
        objectId = bundle.getString("User");
        setInfo(objectId);
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
        navView.setTv_title("咨询师详情");
        navView.getIv_left().setImageResource(R.drawable.ic_keyboard_arrow_left_white_36dp);
//        navView.getIv_right().setImageResource(R.drawable.ic_perm_identity_white_36dp);
        navView.setClickListener(new NavigationView.ClickCallBack() {
            @Override
            public void onLeftClick() {
                finish();
            }

            @Override
            public void onRightClick() {
//                startActivity(new Intent(getBaseContext(), MyActivity.class));
            }
        });
    }


    //设置用户信息
    private void setInfo(String id) {
        UserModel.getInstance().queryUserInfo(id, new QueryUserListener() {
            @Override
            public void done(User s, BmobException e) {
                user=s;
                if (s.getNick()==null&&s.getNick().equals("")){
                    info = new BmobIMUserInfo(s.getObjectId(), s.getUsername(), s.getAvatar());
                }else {
                    info = new BmobIMUserInfo(s.getObjectId(), s.getNick(), s.getAvatar());
                }
                if (!(s.getAvatar().isEmpty())) {
                    ViewUtils.setPic(s.getAvatar(), R.mipmap.head, ivAvatar);
                }
                if (!(s.getNick().isEmpty())) {
                    tvName.setText(s.getNick());
                }
                if (!(s.getJob().isEmpty())) {
                    tvJob.setText(s.getJob());
                }
                if (!(s.getSummary().isEmpty())) {
                    tvGood.setText(s.getSummary());
                }

            }
        });
    }

    public void onEvent(ChatEvent event) {
        BmobIMUserInfo info = event.info;
        //如果需要更新用户资料，开发者只需要传新的info进去就可以了
        Logger.i("" + info.getName() + "," + info.getAvatar() + "," + info.getUserId());
        BmobIM.getInstance().startPrivateConversation(info, new ConversationListener() {
            @Override
            public void done(BmobIMConversation c, BmobException e) {
                if (e == null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("c", c);
                    startActivity(ChatActivity.class, bundle, false);
                } else {
                    toast(e.getMessage() + "(" + e.getErrorCode() + ")");
                }
            }
        });
    }

    @OnClick({R.id.btn_consult, R.id.btn_appointment})
    public void onClick(View view) {
        //检查是否登录
        boolean isLogin=UserModel.getInstance().checkIsLogin(this);
        if (!isLogin){
            CommonUtils.showToast(this,"请先登录！");
            startActivity(new Intent(this,LoginActivity.class));
            return;
        }
        switch (view.getId()) {
            case R.id.btn_consult:
                EventBus.getDefault().post(new ChatEvent(info));
                break;
            case R.id.btn_appointment:
                Intent intent = new Intent(this, MakeOrderActivity.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("User",user);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}
