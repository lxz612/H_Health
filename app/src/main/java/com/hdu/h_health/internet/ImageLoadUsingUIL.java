package com.hdu.h_health.internet;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;

import com.hdu.h_health.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

/**
 * 使用Universal_Image_Loader加载缓存图片
 * Created by 612 on 2016/3/10.
 */
public class ImageLoadUsingUIL {

    public void ImageLoad(Context context,ImageView imageView, String url) {
        ImageLoader.getInstance().displayImage(url, imageView, getOptions());
    }

    /**
     * 显示设置
     *
     * @return
     */
    private DisplayImageOptions getOptions() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.loading) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.loading) // 设置图片加载或解码过程中发生错误显示的图片
                .resetViewBeforeLoading(false)  // default 设置图片在加载前是否重置、复位
                .delayBeforeLoading(1000)  // 下载前的延迟时间
                .cacheInMemory(true) // default  设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // default  设置下载的图片是否缓存在SD卡中
//                .preProcessor(...)
//                .postProcessor(...)
//                .extraForDownloader(...)
                .considerExifParams(false) // default
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default 设置图片以如何的编码方式显示
                .bitmapConfig(Bitmap.Config.ARGB_8888) // default 设置图片的解码类型
//                .decodingOptions(...)  // 图片的解码设置
                .displayer(new SimpleBitmapDisplayer()) // default  还可以设置圆角图片new RoundedBitmapDisplayer(20)
                .handler(new Handler()) // default
                .build();
        return options;
    }
}
