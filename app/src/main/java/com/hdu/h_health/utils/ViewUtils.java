package com.hdu.h_health.utils;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by 612 on 2016/3/20.
 */
public class ViewUtils {

    /**
     * 设置图片（String类型）
     *
     * @param avatar     --图片的Url
     * @param defaultRes --默认图片
     * @param iv         --ImageView
     */
    public static void setPic(String avatar, int defaultRes, ImageView iv) {
        if (!TextUtils.isEmpty(avatar)) {//如果URL为空
            if (!avatar.equals(iv.getTag())) {//增加tag标记，减少UIL的display次数
                iv.setTag(avatar);
                //不直接display imageview改为ImageAware，解决ListView滚动时重复加载图片
                ImageAware imageAware = new ImageViewAware(iv, false);
                ImageLoader.getInstance().displayImage(avatar, imageAware, DisplayConfig.getDefaultOptions(true, defaultRes));
            }
        } else {
            iv.setImageResource(defaultRes);
        }
    }

    /**
     * 显示图片
     *
     * @param url
     * @param defaultRes
     * @param iv
     * @param listener
     */
    public static void setPicture(String url, int defaultRes, ImageView iv, ImageLoadingListener listener) {
        if (!TextUtils.isEmpty(url)) {
            if (!url.equals(iv.getTag())) {//增加tag标记，减少UIL的display次数
                iv.setTag(url);
                //不直接display imageview改为ImageAware，解决ListView滚动时重复加载图片
                ImageAware imageAware = new ImageViewAware(iv, false);
                if (listener != null) {
                    ImageLoader.getInstance().displayImage(url, imageAware, DisplayConfig.getDefaultOptions(false, defaultRes), listener);
                } else {
                    ImageLoader.getInstance().displayImage(url, imageAware, DisplayConfig.getDefaultOptions(false, defaultRes));
                }
            }
        } else {
            iv.setImageResource(defaultRes);
        }
    }

    //根据URL获取Bitmap
    public static Bitmap getBitmap(String url, int defaultRes){
        Bitmap bitmap;
        if (!TextUtils.isEmpty(url)) {//如果不URL为空
            ImageSize imageSize=new ImageSize(50,50,0);
            bitmap = ImageLoader.getInstance().loadImageSync(url);
            return bitmap;
        }
        return null;
    }
}
