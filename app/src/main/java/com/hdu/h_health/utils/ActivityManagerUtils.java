package com.hdu.h_health.utils;


import android.app.Activity;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Activity的搜集和释放工具类
 * Created by 612 on 2016/3/20.
 */
public class ActivityManagerUtils {

    private List<Activity> activityList = new LinkedList<Activity>();

    private static ActivityManagerUtils activityManagerUtils;

    public ActivityManagerUtils() {

    }

    public static ActivityManagerUtils getInstance() {
        if (activityManagerUtils == null) {
            return new ActivityManagerUtils();
        } else {
            return activityManagerUtils;
        }
    }

    //获取顶部的Activity
    public Activity getTopActivity() {
        return activityList.get(activityList.size() - 1);
    }

    //添加顶部的Activity
    public void addActivity(Activity ac) {
        Log.i("Main", "addActivity: ");
        activityList.add(ac);
    }

    //TODO 移除所有的Activity
    public void removeAllActivity() {
        Log.i("Main", "removeAllActivity");
        for (Activity ac : activityList) {
            if (null != ac) {
                Log.i("Main", "removeAllActivity: 不为空");
                if (!ac.isFinishing()) {
                    ac.finish();
                    Log.i("Main", "removeAllActivity: 结束");
                }
                ac = null;
            } else {
                Log.i("Main", "removeAllActivity: 为空");

            }
        }
        activityList.clear();
    }
}
