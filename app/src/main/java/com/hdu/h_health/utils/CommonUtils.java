package com.hdu.h_health.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * 通知工具类
 * Created by 612 on 2016/3/22.
 */
public class CommonUtils {

    public static void showToast(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
    }

    public static void showLog(String string) {
        Log.i("Main", string);
    }
}
