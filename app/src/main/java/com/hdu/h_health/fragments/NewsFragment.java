package com.hdu.h_health.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hdu.h_health.MyApplication;
import com.hdu.h_health.news.widget.Fragment1;
import com.hdu.h_health.news.widget.NewsListFragment;
import com.hdu.h_health.R;
import com.hdu.h_health.dependence.SlidingTab.SlidingTabLayout;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * 资讯视图
 * Created by 612 on 2016/4/17.
 */
public class NewsFragment extends Fragment {

    //文章类别
    public static final int NEWS_TYPE_1 = 1;
    public static final int NEWS_TYPE_2 = 2;
    public static final int NEWS_TYPE_3 = 3;
    public static final int NEWS_TYPE_4 = 4;
    public static final int NEWS_TYPE_5 = 5;
    public static final int NEWS_TYPE_6 = 6;
    public static final int NEWS_TYPE_7 = 7;
    public static final int NEWS_TYPE_8 = 8;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, null);
        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(4);
        setupViewPager(mViewPager);
        //设置SlidingTabLayout与ViewPager的连接
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.slidingTabLayout);
        slidingTabLayout.setViewPager(mViewPager);
        return view;
    }

    private void setupViewPager(ViewPager mViewPager) {
        //Fragment中嵌套使用Fragment一定要使用getChildFragmentManager(),否则会有问题
        MyPagerAdapter adapter = new MyPagerAdapter(getChildFragmentManager());
        adapter.addFragment(Fragment1.newInstance(NEWS_TYPE_1), getString(R.string.Fragment1_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_2), getString(R.string.Fragment2_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_3), getString(R.string.Fragment3_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_4), getString(R.string.Fragment4_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_5), getString(R.string.Fragment5_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_6), getString(R.string.Fragment6_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_7), getString(R.string.Fragment7_txt));
        adapter.addFragment(NewsListFragment.newInstance(NEWS_TYPE_8), getString(R.string.Fragment8_txt));
        mViewPager.setAdapter(adapter);
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> titles = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //添加Fragment
        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            titles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MyApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }
}
