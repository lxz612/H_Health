package com.hdu.h_health.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.hdu.h_health.Adapter.SecretAdapter;
import com.hdu.h_health.MyApplication;
import com.hdu.h_health.R;
import com.hdu.h_health.bean.Secret;
import com.hdu.h_health.dependence.xlistview.XListView;
import com.hdu.h_health.model.SecretModel;
import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.ui.CommentActivity;
import com.hdu.h_health.ui.EditSecretActivity;
import com.hdu.h_health.ui.LoginActivity;
import com.hdu.h_health.ui.MyActivity;
import com.hdu.h_health.utils.CommonUtils;
import com.hdu.h_health.widget.NavigationView;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.listener.FindListener;

/**
 * 心事界面
 * Created by See on 2016/2/25.
 */
public class PrivateFragment extends Fragment implements XListView.IXListViewListener, AdapterView.OnItemClickListener {
    @Bind(R.id.listView)
    XListView listView;

    private List<Secret> secrets = new ArrayList<Secret>();
    private SecretAdapter secretAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_private, container, false);
        initView(view);
        secretAdapter = new SecretAdapter(getActivity(), secrets);
        listView.setAdapter(secretAdapter);
        listView.setXListViewListener(this);
        listView.setOnItemClickListener(this);
        //设置上拉加载
        listView.setPullLoadEnable(true);
        query(0, STATE_REFRESH);
        return view;
    }

    private void initView(View view) {
        ButterKnife.bind(this, view);
    }

    private int curPage = 0;// 当前页的编号，从0开始

    //下拉加载
    @Override
    public void onRefresh() {
        curPage = 0;
        query(curPage, STATE_REFRESH);
    }

    //上拉加载
    @Override
    public void onLoadMore() {
        curPage++;
        query(curPage, STATE_MORE);
    }

    //停止下拉刷新和上拉加载
    private void stopLoad() {
        listView.stopRefresh();
        listView.stopLoadMore();
        listView.setRefreshTime(R.string.xlistview_time);//设置刷新时间
    }

    private static final int STATE_REFRESH = 0;// 下拉刷新
    private static final int STATE_MORE = 1;// 加载更多

    //查询心事
    private void query(int page, final int type) {
        SecretModel.getInstance().getAllSecret(getActivity(), page, new FindListener<Secret>() {
            @Override
            public void onSuccess(List<Secret> list) {
                CommonUtils.showLog("查询成功");
                if (list.size() >= 0) {
                    if (type == STATE_REFRESH) {
                        secrets.clear();
                    }
                    for (Secret se : list) {
                        secrets.add(se);
                    }
                    secretAdapter.notifyDataSetChanged();
                } else if (type == STATE_MORE) {
                    CommonUtils.showToast(getActivity(), "没有更多心事");
                } else if (type == STATE_REFRESH) {
                    CommonUtils.showToast(getActivity(), "暂无心事");
                }
                stopLoad();
            }

            @Override
            public void onError(int i, String s) {
                CommonUtils.showToast(getActivity(), "查询失败");
                stopLoad();
            }
        });
    }

    //列表项点击，传递secret对象
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Secret secret = secrets.get(position - 1);
        Intent intent = new Intent(getActivity(), CommentActivity.class);
        intent.putExtra("secret", secret);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MyApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }
}
