package com.hdu.h_health;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;

import com.hdu.h_health.utils.ViewUtils;

/**
 * ImageView创建工厂
 */
public class ViewFactory {

    /**
     * 获取ImageView视图的同时加载显示url
     *
     * @return
     */
    public static ImageView getImageView(Context context, String url) {
        ImageView imageView = (ImageView) LayoutInflater.from(context).inflate(
                R.layout.view_banner, null);
        ViewUtils.setPic(url, R.drawable.loading, imageView);
//		ImageLoader.getInstance().displayImage(url, imageView);
        return imageView;
    }
}
