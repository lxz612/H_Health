package com.hdu.h_health.news.presenter;

import android.content.Context;

/**
 * MVP-Presenter接口
 * Created by 612 on 2016/3/27.
 */
public interface InfoPresenter {
    void loadInfo(Context context, int page,int type);
}
