package com.hdu.h_health.news.view;

import java.util.List;

/**
 * MVP-View层接口（这是与presenter约定好的接口，供其调用）
 * Created by 612 on 2016/3/29.
 */
public interface BaseView<T>{
    void showProgress();
    void addData(List<T> newsList);
    void hideProgress();
    void showLoadFailMsg(String error);
    void changePage(int page);
}
