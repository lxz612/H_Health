package com.hdu.h_health.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.NewsBean;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 除精选之外资讯布局的适配器(带上拉加载)
 * Created by 612 on 2016/4/17.
 */
public class FragmentXAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;//列表项

    private static final int TYPE_FOOTER = 1;//上拉加载

    private boolean mShowFooter = false;//显示底部上拉加载布局

    private LayoutInflater inflater;//布局解析器

    private List<NewsBean> mNewsData;//数据

    private Context context;

    public FragmentXAdapter(Context context, List<NewsBean> mNewsData) {
        this.context = context;
        this.mNewsData = mNewsData;
        inflater = LayoutInflater.from(context);
    }

    //获取对应Item的布局
    @Override
    public int getItemViewType(int position) {
        // 将最后一个item设置为footerView
        if (!mShowFooter) {
            return TYPE_ITEM;
        }
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = inflater.inflate(R.layout.item_list, parent, false);
            return new ItemViewHolder(view);
        } else {
            View footer = inflater.inflate(R.layout.footer, parent, false);
            footer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            return new FooterViewHolder(footer);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ViewUtils.setPic(mNewsData.get(position).getPicurl(), R.drawable.loading,
                    ((ItemViewHolder) holder).ivImg);
            ((ItemViewHolder) holder).tvTitle.setText(mNewsData.get(position).getTitle());
            ((ItemViewHolder) holder).tvFrom.setText(mNewsData.get(position).getContent());
        }
    }

    //返回Item的数量
    @Override
    public int getItemCount() {
        int begin = mShowFooter ? 1 : 0;
        if (mNewsData == null) {//列表没有数据时，不应该显示底部布局，故返回值为0
//            return begin;
            return 0;
        }
        return mNewsData.size() + begin;
    }

    //提供接口设置是否显示底部加载布局
    public void isShowFooter(boolean showFooter) {
        this.mShowFooter = showFooter;
    }

    //提供接口返回底部加载布局的状态
    public boolean isShowFooter() {
        return this.mShowFooter;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.iv_img)
        ImageView ivImg;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_from)
        TextView tvFrom;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //设置点击事件
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, this.getAdapterPosition());
            }
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view) {
            super(view);
        }
    }

    //点击事件回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    //外界使用接口
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
}
