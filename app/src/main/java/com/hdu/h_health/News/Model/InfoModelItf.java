package com.hdu.h_health.news.model;

import android.content.Context;

import com.hdu.h_health.bean.NewsBean;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;

/**
 * MVP-Model层
 * Created by 612 on 2016/3/27.
 */
public class InfoModelItf implements InfoModel {

    //请求每页数量
    private final static int LIMIT = 5;

    //向Bmob后台请求数据
    //参数：上下文，请求开始页，请求数据类型，回调接口
    @Override
    public void queryInfo(Context context, int page, int type,
                          final OnLoadListListener listListener) {
        BmobQuery<NewsBean> query = new BmobQuery<>();
        query.setLimit(LIMIT);
        query.setSkip(page * LIMIT);
        if (type!=1){
            query.addWhereEqualTo("type", type);
        }
        query.order("-createdAt");
        query.findObjects(context, new FindListener<NewsBean>() {
            @Override
            public void onSuccess(List<NewsBean> list) {
                listListener.success(list);
            }

            @Override
            public void onError(int i, String s) {
                listListener.onFailure(i + s);
            }
        });
    }

    //定义回调接口（这个相当于c和javascript中的“回调函数”）
    public interface OnLoadListListener {
        void success(List<NewsBean> list);
        void onFailure(String msg);
    }
}
