package com.hdu.h_health.news.model;

import android.content.Context;

/**
 * MVP-Model层接口
 * Created by 612 on 2016/3/27.
 */
public interface InfoModel {
    void queryInfo(Context context, final int page,int type,
                   InfoModelItf.OnLoadListListener listener);
}
