package com.hdu.h_health.news.presenter;

import android.content.Context;

import com.hdu.h_health.news.view.BaseView;
import com.hdu.h_health.news.model.InfoModel;
import com.hdu.h_health.news.model.InfoModelItf;
import com.hdu.h_health.bean.NewsBean;

import java.util.List;

/**
 * MVP-Presenter层（所有与View变动有关的逻辑都写在这里）
 * Created by 612 on 2016/3/27.
 */
public class InfoPresenterItf implements InfoPresenter, InfoModelItf.OnLoadListListener {

    private InfoModel infoModel;//Model层的接口对象
    private BaseView baseView;//View层的接口对象
    private int currentPage;//当前加载页

    public InfoPresenterItf(BaseView baseView) {
        this.baseView = baseView;
        this.infoModel = new InfoModelItf();
    }

    //加载数据
    @Override
    public void loadInfo(Context context, int page, int type) {
        if (page == 0) {
            //下拉刷新
            baseView.showProgress();
        }
        //调用Model层
        infoModel.queryInfo(context, page, type, this);
        this.currentPage =page;
    }

    //从Model层获取数据
    //success和failure中的代码 就是 传给Model的回调函数
    @Override
    public void success(List<NewsBean> list) {
        baseView.hideProgress();
        if (list.size() >=0) {
            baseView.addData(list);
        }
        //更新当前页
        currentPage++;
        baseView.changePage(currentPage);
    }

    @Override
    public void onFailure(String msg) {
        baseView.hideProgress();
        baseView.showLoadFailMsg(msg);
    }
}
