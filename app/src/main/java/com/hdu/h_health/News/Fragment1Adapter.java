package com.hdu.h_health.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.NewsBean;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 可设置头部布局和带底部刷新的RecyclerViewAdapter
 * Created by 612 on 2016/4/17.
 */
public class Fragment1Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 0;//头部布局
    public static final int TYPE_ITEM = 1;//列表项布局
    private static final int TYPE_FOOTER = 2;//上拉加载布局

    private View headView;//头部View
    private boolean mShowFooter;//是否显示Footer

    private List<NewsBean> mNewsData;
    private LayoutInflater inflater;
    private Context context;

    public Fragment1Adapter(Context context, List<NewsBean> mNewsData) {
        this.context = context;
        this.mNewsData = mNewsData;
        inflater = LayoutInflater.from(context);
    }

    //设置headView
    public void setHeaderView(View headerView) {
        this.headView = headerView;
        notifyItemInserted(0);
    }

    //获得headView
    public View getHeaderView() {
        return headView;
    }

    //是否显示Footer
    public void isShowFooter(boolean showFooter) {
        this.mShowFooter = showFooter;
    }

    //获取Footer状态
    public boolean isShowFooter() {
        return this.mShowFooter;
    }

    @Override
    public int getItemViewType(int position) {
        if (headView != null && position == 0) {
            return TYPE_HEADER;
        }
        if (position == mNewsData.size() + (headView != null ? 1 : 0) && isShowFooter()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(headView);
        } else if (viewType == TYPE_FOOTER) {
            View footer = inflater.inflate(R.layout.footer, parent, false);
            footer.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            return new FooterViewHolder(footer);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list, parent, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_HEADER) return;
        int pos = getRealPosition(holder);
        if (holder instanceof ItemViewHolder) {
            ViewUtils.setPic(mNewsData.get(pos).getPicurl(), R.drawable.loading,
                    ((ItemViewHolder) holder).ivImg);
            ((ItemViewHolder) holder).tvTitle.setText(mNewsData.get(pos).getTitle());
            ((ItemViewHolder) holder).tvFrom.setText(mNewsData.get(pos).getContent());
        }
    }

    //获取position
    public int getRealPosition(RecyclerView.ViewHolder holder) {
        int position = holder.getLayoutPosition();
        return headView == null ? position : position - 1;
    }

    @Override
    public int getItemCount() {
        return mNewsData.size() + (headView != null ? 1 : 0) + (isShowFooter() ? 1 : 0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.iv_img)
        ImageView ivImg;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_from)
        TextView tvFrom;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //设置点击事件
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, this.getAdapterPosition());
            }
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
//            if (mOnItemClickListener != null) {
//                mOnItemClickListener.onItemClick(itemView, this.getAdapterPosition());
//            }
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view) {
            super(view);
        }
    }

    //列表项点击事件回调接口
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    //外界使用接口
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
}
