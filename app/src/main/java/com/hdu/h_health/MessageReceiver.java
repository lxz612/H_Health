package com.hdu.h_health;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.hdu.h_health.model.UserModel;
import com.hdu.h_health.model.i.UpdateCacheListener;
import com.hdu.h_health.ui.ChatActivity;
import com.hdu.h_health.ui.MainActivity;
import com.hdu.h_health.utils.ViewUtils;
import com.orhanobut.logger.Logger;

import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMMessageType;
import cn.bmob.newim.bean.BmobIMUserInfo;
import cn.bmob.newim.event.MessageEvent;
import cn.bmob.newim.notification.BmobNotificationManager;
import cn.bmob.v3.exception.BmobException;

/**
 * 广播接收器--接收从Bmob服务器发来的消息 （NewIM_V2.0.1）
 * Created by 612 on 2016/3/8.
 */
public class MessageReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent != null) {
            //MessageEvent--接收消息的事件，内部采用EventBus传递消息
            final MessageEvent event = (MessageEvent) intent.getSerializableExtra("event");
            Logger.i(event.getConversation().getConversationTitle() + "," + event.getMessage().getMsgType() + "," + event.getMessage().getContent());
            //是否显示notification，大于0,则不显示
            if (BmobNotificationManager.getInstance().isShowNotification()) {
                //更新下用户的信息
                UserModel.getInstance().updateUserInfo(event, new UpdateCacheListener() {
                    @Override
                    public void done(BmobException e) {
                        Intent pendingIntent = new Intent(context, MainActivity.class);
                        pendingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        BmobIMMessage msg = event.getMessage();
                        //用户自定义的消息类型，其类型值均为0，因此在这里开发者可自行处理自定义的消息类型
                        if (BmobIMMessageType.getMessageTypeValue(msg.getMsgType()) < 1) {
                            //自行处理自定义消息类型
                            Logger.i(msg.getMsgType() + "," + msg.getContent() + "," + msg.getExtra());
                        } else {//SDK支持的消息类型可使用以下方法展示通知
                            //多个用户的多条消息合并成一条通知：有XX个联系人发来了XX条消息
                            BmobNotificationManager.getInstance().showNotification(event, pendingIntent);
//                            //自定义通知消息：始终只有一条通知，新消息覆盖旧消息
//                            BmobIMMessage msg =event.getMessage();
//                            BmobIMUserInfo info =event.getFromUserInfo();
//                            //将聊天头像转成bitmap
//                            Bitmap largeIcon = ViewUtils.getBitmap(info.getAvatar(),R.mipmap.ic_launcher);
//                            //显示通知
//                            BmobNotificationManager.getInstance().showNotification(largeIcon,info.getName(),msg.getContent(),"您有一条新消息",pendingIntent);
                        }
                    }
                });
            } else {
                Logger.i("处于应用界面中,不需要显示通知");
            }
        }
    }
}
