package com.hdu.h_health.Adapter;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * 接收到的文本类型
 * Created by 612 on 2016/3/9.
 */
public class ReceiveTextViewHolder {
    TextView tv_time,tv_message;
    ImageView iv_avatar;
}
