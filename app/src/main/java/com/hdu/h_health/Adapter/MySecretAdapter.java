package com.hdu.h_health.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.Secret;
import com.hdu.h_health.common.CommonViewHolder;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

/**
 * 我的心事适配器
 * Created by 612 on 2016/3/23.
 */
public class MySecretAdapter extends BaseAdapter {
    private Context context;
    private List<Secret> secrets;

    public MySecretAdapter(Context context, List<Secret> secrets) {
        this.context = context;
        this.secrets = secrets;
    }

    @Override
    public int getCount() {
        return secrets.size();
    }

    @Override
    public Object getItem(int position) {
        return secrets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonViewHolder common = CommonViewHolder.get(context, convertView, parent, R.layout.item_content, position);
        ImageView avatar = common.getView(R.id.iv_avatar);
        TextView user_name = common.getView(R.id.username);
        TextView content_text = common.getView(R.id.tv_content);
        ViewUtils.setPic(secrets.get(position).getAuthor().getAvatar(), R.mipmap.head, avatar);
        if (secrets.get(position).getAuthor().getNick().isEmpty()){
            user_name.setText(secrets.get(position).getAuthor().getUsername());
        }else {
            user_name.setText(secrets.get(position).getAuthor().getNick());
        }
        content_text.setText(secrets.get(position).getContent());
        return common.getConvertView();
    }
}
