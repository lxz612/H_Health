package com.hdu.h_health.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.Comment;
import com.hdu.h_health.common.CommonViewHolder;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

/**
 * 心事评论适配器
 * Created by 612 on 2016/3/24.
 */
public class CommentAdapter extends BaseAdapter {
    private Context context;
    private List<Comment> comments;
    public CommentAdapter(Context context,List<Comment> comments){
        this.comments=comments;
        this.context=context;
    }
    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonViewHolder common=CommonViewHolder.get(context,convertView,parent, R.layout.item_comment,position);
        ImageView imageView=common.getView(R.id.iv_avatar);
        TextView nickname=common.getView(R.id.nickname);
        TextView content=common.getView(R.id.content);
        TextView time=common.getView(R.id.time);
        ViewUtils.setPic(comments.get(position).getAuthor().getAvatar(),R.mipmap.head,imageView);
        if(comments.get(position).getAuthor().getType()==3){
            nickname.setTextColor(Color.parseColor("#63B8FF"));
        }
        if (!comments.get(position).getAuthor().getNick().equals("")){
            nickname.setText(comments.get(position).getAuthor().getNick());
        }else {
            nickname.setText(comments.get(position).getAuthor().getUsername());
        }
        content.setText(comments.get(position).getContent());
        time.setText(comments.get(position).getCreatedAt());
        return common.getConvertView();
    }
}
