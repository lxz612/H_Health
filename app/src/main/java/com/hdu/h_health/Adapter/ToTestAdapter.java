package com.hdu.h_health.Adapter;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.ToTestBean;

import java.util.List;

import cn.bmob.v3.BmobObject;

/**
 * Created by See on 2016/3/23.
 */
public class ToTestAdapter extends BaseAdapter {
    private List<BmobObject> testBeans;
    private LayoutInflater mInflater;
    Handler mHandler;
    public static int countA = 0;
    public static int countB = 0;
    public static int countC = 0;


    public ToTestAdapter(Context context, List<BmobObject> list) {
        mInflater = LayoutInflater.from(context);
        testBeans = list;
    }

    @Override
    public int getCount() {
        return testBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return testBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ToTestBean bean = (ToTestBean) testBeans.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.totest_item, null);
            holder.question = (TextView) convertView.findViewById(R.id.totest_question);
            holder.option1 = (RadioButton) convertView.findViewById(R.id.totest_option1);
            holder.option2 = (RadioButton) convertView.findViewById(R.id.totest_option2);
            holder.option3 = (RadioButton) convertView.findViewById(R.id.totest_option3);
            holder.radioGroup = (RadioGroup) convertView.findViewById(R.id.totest_radiogroup);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder.question.setText(bean.question);
        holder.option1.setText(bean.option1);
        holder.option2.setText(bean.option2);
        holder.option3.setText(bean.option3);

        holder.radioGroup.setId(position);
        holder.radioGroup.setOnCheckedChangeListener(null);
        if (!TextUtils.isEmpty(bean.getAnswer()))
        {
            if (bean.getAnswer().equals("A"))
            {
                holder.radioGroup.check(R.id.totest_option1);
                countA++;
            }
            else if (bean.getAnswer().equals("B")) {
                holder.radioGroup.check(R.id.totest_option2);
                countB++;
            }
                else if (bean.getAnswer().equals("C"))
            {
                holder.radioGroup.check(R.id.totest_option3);
                countC++;
            }
            else {
                holder.radioGroup.clearCheck();
            }}
            else {
                holder.radioGroup.clearCheck();
            }
            holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ToTestBean item = (ToTestBean) testBeans.get(group.getId());
                if (item == null) return;
                switch (checkedId) {
                    case R.id.totest_option1: {
                        item.setAnswer("A");
                        break;
                    }
                    case R.id.totest_option2: {
                        item.setAnswer("B");
                        break;
                    }
                    case R.id.totest_option3:

                        item.setAnswer("C");
                        break;
                }
            }
        });
//            holder.option1.setText(bean.option1);
//            holder.option2.setText(bean.option2);
//            holder.option3.setText(bean.option3);
        return convertView;
    }

    private class ViewHolder {
        public TextView question;
        public RadioButton option1;
        public RadioButton option2;
        public RadioButton option3;
        public RadioGroup radioGroup;
    }


//    public void RadioGroupThread(final RadioGroup radioGroup, final ToTestBean bean) {
//        mHandler =  new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                ToTestBean holder = (ToTestBean) msg.obj;
//                if(holder.radioGroup.getTag().equals(holder.url)) {
//                    holder.radioGroup.clearCheck();
//                }
//            }
//        };
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//            }
//        }
//    }
}
