package com.hdu.h_health.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.Order;
import com.hdu.h_health.common.CommonViewHolder;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

/**
 * 我的订单适配器
 * Created by 612 on 2016/4/4.
 */
public class MyOrderAdapter extends BaseAdapter {
    private Context context;
    private List<Order> orders;
    private Order order;

    public MyOrderAdapter(Context context, List<Order> orders) {
        this.context = context;
        this.orders = orders;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonViewHolder viewHolder = CommonViewHolder.get(context, convertView, parent, R.layout.item_order, position);
        TextView tvOrdreNumber = viewHolder.getView(R.id.tv_orderNumber);
        TextView tvTime = viewHolder.getView(R.id.tv_time);
        TextView tvType = viewHolder.getView(R.id.tv_type);
        ImageView iv = viewHolder.getView(R.id.iv);
        TextView tvName = viewHolder.getView(R.id.tv_name);
        TextView tvContype = viewHolder.getView(R.id.tv_contype);
        TextView tvSum = viewHolder.getView(R.id.tv_sum);
        TextView tvAcount = viewHolder.getView(R.id.tv_acount);
        order = orders.get(position);
        tvOrdreNumber.setText(order.getObjectId());
        tvTime.setText(order.getCreatedAt());
        if (order.getType() == 0) {
            tvType.setText("待付款");
        } else if (order.getType() == 1) {
            tvType.setText("已付款");
        } else if (order.getType() == 2) {
            tvType.setText("失效订单");
        } else if (order.getType() == 3) {
            tvType.setText("待评价");
        }
        ViewUtils.setPic(order.getCounsult().getAvatar(),R.mipmap.head,iv);
        tvName.setText(order.getCounsult().getNick());
        if (order.getConType()==1){
            tvContype.setText("语音咨询");
        }else if(order.getConType()==2){
            tvContype.setText("视频咨询");
        }else if (order.getConType()==3){
            tvContype.setText("面对面咨询");
        }
        tvSum.setText(order.getSum()+"次");
        tvAcount.setText(order.getAcount()+"元");
        return viewHolder.getConvertView();
    }
}
