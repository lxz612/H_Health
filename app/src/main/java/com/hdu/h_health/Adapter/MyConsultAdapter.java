package com.hdu.h_health.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.utils.TimeUtil;
import com.hdu.h_health.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.newim.BmobIM;
import cn.bmob.newim.bean.BmobIMConversation;
import cn.bmob.newim.bean.BmobIMMessage;
import cn.bmob.newim.bean.BmobIMMessageType;

/**
 * 我的咨询列表适配器
 * Created by 612 on 2016/3/9.
 */
public class MyConsultAdapter extends RecyclerView.Adapter<MyConsultAdapter.MyConsultViewHolder> {

    private List<BmobIMConversation> conversations = new ArrayList<BmobIMConversation>();

    private Context context;

    private LayoutInflater inflater;

    public MyConsultAdapter(Context context, List<BmobIMConversation> conversations) {
        this.context = context;
        this.conversations = conversations;
        inflater = LayoutInflater.from(context);
    }

    //注册点击事件监听的接口
    public interface onItemClickListener {

        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private onItemClickListener onItemClickListener;

    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    /**
     * 移除会话
     *
     * @param position
     */
    public void remove(int position) {
        conversations.remove(position);
        notifyDataSetChanged();
    }

    /**
     * 重新更新会话列表
     *
     * @param conver
     */
    public void change(List<BmobIMConversation> conver) {
        conversations.removeAll(conversations);
        conversations.addAll(conver);
        notifyDataSetChanged();
    }

    /**
     * 获取会话
     *
     * @param position
     * @return
     */
    public BmobIMConversation getItem(int position) {
        return conversations.get(position);
    }

    /**
     * 创建新的View，被LayoutManager所调用
     */
    @Override
    public MyConsultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyConsultViewHolder(inflater.inflate(R.layout.item_conversation, parent, false));
    }

    /**
     * 将数据与界面进行绑定的操作
     */
    @Override
    public void onBindViewHolder(final MyConsultViewHolder holder, int position) {
        BmobIMConversation conversation = conversations.get(position);
        List<BmobIMMessage> msgs = conversation.getMessages();
        if (msgs != null && msgs.size() > 0) {
            BmobIMMessage lastMsg = msgs.get(0);
            String content = lastMsg.getContent();
            if (lastMsg.getMsgType().equals(BmobIMMessageType.TEXT.getType())) {//最新的消息是文本的话
                holder.tv_recent_msg.setText(content);
            } else if (lastMsg.getMsgType().equals(BmobIMMessageType.IMAGE.getType())) {//图片
                holder.tv_recent_msg.setText("[图片]");
            } else if (lastMsg.getMsgType().equals(BmobIMMessageType.VOICE.getType())) {//声音
                holder.tv_recent_msg.setText("[语音]");
            } else if (lastMsg.getMsgType().equals(BmobIMMessageType.LOCATION.getType())) {//位置
                holder.tv_recent_msg.setText("[位置]" + content);
            } else {//开发者自定义的消息类型，需要自行处理
                holder.tv_recent_msg.setText("[未知]");
            }
            holder.tv_recent_time.setText(TimeUtil.getChatTime(false, lastMsg.getCreateTime()));
        }
        //会话图标
        ViewUtils.setPic(conversation.getConversationIcon(), R.mipmap.head, holder.iv_recent_avatar);
        //会话标题
        holder.tv_recent_name.setText(conversation.getConversationTitle());
        //查询指定未读消息数
        long unread = BmobIM.getInstance().getUnReadCount(conversation.getConversationId());
        if (unread > 0) {
            holder.tv_recent_unread.setVisibility(View.VISIBLE);
            holder.tv_recent_unread.setText(String.valueOf(unread));
        } else {
            holder.tv_recent_unread.setVisibility(View.GONE);
        }
        // 如果设置了回调，则设置点击事件
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    onItemClickListener.onItemClick(holder.itemView, pos);
                }
            });
            /*返回值为true，只处理长按时间*/
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int pos = holder.getLayoutPosition();
                    onItemClickListener.onItemLongClick(holder.itemView, pos);
                    return true;
                }
            });
        }
    }

    //数据项数量
    @Override
    public int getItemCount() {
        return conversations == null ? 0 : conversations.size();
    }

    public static class MyConsultViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_recent_avatar;
        TextView tv_recent_name, tv_recent_msg, tv_recent_time, tv_recent_unread;

        public MyConsultViewHolder(View itemView) {
            super(itemView);
            iv_recent_avatar = (ImageView) itemView.findViewById(R.id.iv_recent_avatar);
            tv_recent_name = (TextView) itemView.findViewById(R.id.tv_recent_name);
            tv_recent_msg = (TextView) itemView.findViewById(R.id.tv_recent_msg);
            tv_recent_time = (TextView) itemView.findViewById(R.id.tv_recent_time);
            tv_recent_unread = (TextView) itemView.findViewById(R.id.tv_recent_unread);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
