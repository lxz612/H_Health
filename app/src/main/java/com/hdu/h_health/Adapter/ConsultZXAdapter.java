package com.hdu.h_health.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.User;
import com.hdu.h_health.common.CommonViewHolder;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

/**
 * 心理咨询师适配器
 * Created by 612 on 2016/3/19.
 */
public class ConsultZXAdapter extends BaseAdapter{
    private Context context;
    private List<User> users;
    public ConsultZXAdapter(Context context,List<User> users){
        this.context=context;
        this.users=users;
    }
    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonViewHolder common=CommonViewHolder.get(context, convertView, parent, R.layout.item_consultzx, position);
        ImageView avatar=common.getView(R.id.iv_avatar);
        TextView name=common.getView(R.id.name);
        TextView appellation=common.getView(R.id.appellation);
        TextView summary=common.getView(R.id.summary);
        ViewUtils.setPic(users.get(position).getAvatar(),R.mipmap.head,avatar);
        name.setText(users.get(position).getNick());
        appellation.setText(users.get(position).getJob());
        summary.setText(users.get(position).getSummary());
        return common.getConvertView();
    }
}
