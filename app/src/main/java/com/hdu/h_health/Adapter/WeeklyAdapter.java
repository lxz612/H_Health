package com.hdu.h_health.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdu.h_health.R;
import com.hdu.h_health.bean.WeeklyBean;
import com.hdu.h_health.utils.ViewUtils;

import java.util.List;

/**
 * 周刊列表适配器
 * Created by See on 2016/4/5.
 */
public class WeeklyAdapter extends BaseAdapter{
    private List<WeeklyBean> listBeans;
    private LayoutInflater layoutInflater;

    public WeeklyAdapter(Context context, List<WeeklyBean> list) {
        listBeans = list;
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return listBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return listBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_consult_weekly, null);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.iv_pic);
            viewHolder.title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        WeeklyBean bean = listBeans.get(position);
//        viewHolder.imageView.setImageResource(bean.getPicurl());
        viewHolder.title.setText(bean.getTitle());
        ViewUtils.setPic(bean.getPicUrl(), R.drawable.loading, viewHolder.imageView);
        return convertView;
    }

    private class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }
}
