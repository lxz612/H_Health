package com.hdu.h_health.Adapter;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * 发送的文本类型
 * Created by 612 on 2016/3/9.
 */
public class SendTextViewHolder {
    TextView tv_time,tv_message,tv_send_status;
    ImageView iv_avatar,iv_fail_resend;
    ProgressBar progress_load;
}
