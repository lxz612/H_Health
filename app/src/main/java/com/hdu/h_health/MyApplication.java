package com.hdu.h_health;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.hdu.h_health.bean.User;
import com.hdu.h_health.utils.ActivityManagerUtils;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;


import java.io.File;

import cn.bmob.newim.BmobIM;

/**
 * 自定义的Application对象
 * Created by 612 on 2016/2/29.
 */
public class MyApplication extends Application {
    public User user;
    private static MyApplication myApplication;

    public static MyApplication getInstance(){
        if (null==myApplication){
            return new MyApplication();
        }else {
            return myApplication;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化
        Logger.init("612");
        //im初始化，SDK初始化
        BmobIM.init(this);
        myApplication = this;
        user = new User();
        initImageLoader(this);

        refWatcher= LeakCanary.install(this);
    }

    //配置和初始化uim
    public static void initImageLoader(Context context) {
        //缓存文件的目录
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "imageloader/Cache");
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800)// default = device screen dimensions(手机屏幕大小) 内存缓存文件的最大长宽
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))//可以通过自己的内存缓存实现
                .memoryCacheSize(2 * 1024 * 1024) // 内存缓存的最大值
                .threadPoolSize(3)//线程池内加载的数量
                .threadPriority(Thread.NORM_PRIORITY - 2)//default 设置当前线程的优先级
                .memoryCacheSizePercentage(13)// default
                .diskCache(new UnlimitedDiskCache(cacheDir))//设置缓存目录
                .diskCacheSize(50 * 1024 * 1024)// 50 MiB SD卡(本地)缓存的最大值
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())//default为使用HASHCODE对UIL进行加密命名,这里用MD5(new Md5FileNameGenerator())加密
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs(); //打印debug log
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public void addActivity(Activity ac) {
        ActivityManagerUtils.getInstance().addActivity(ac);
    }

    public void exit() {
        ActivityManagerUtils.getInstance().removeAllActivity();
    }

    public Activity getTopActivity() {
        return ActivityManagerUtils.getInstance().getTopActivity();
    }

    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context){
        MyApplication application= (MyApplication) context.getApplicationContext();
        return application.refWatcher;
    }


}
